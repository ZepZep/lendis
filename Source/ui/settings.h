#ifndef SETTINGS_H
#define SETTINGS_H

#include <QMainWindow>
#include <QSettings>
#include <QDir>
#include <QFile>

namespace Ui {
class Settings;
}

class Settings : public QMainWindow
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();
    void autochooseNN();

private:
    Ui::Settings *ui;
    void closeEvent(QCloseEvent *);

    void loadSettings();
    void loadNNs();

    QDir *dir;

    QSettings *presets;

signals:
    void save();
    void nnChanged(QString path);

private slots:
    void on_pBSave_clicked();
    void on_nnSelector_currentIndexChanged(const QString &arg1);
    void on_groupBoxSimpleDet_toggled(bool arg1);
    void on_groupBoxNN_toggled(bool arg1);
};

#endif // SETTINGS_H

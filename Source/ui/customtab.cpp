#include "customtab.h"
#include "ui_customtab.h"

CustomTab::CustomTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CustomTab)
{
    ui->setupUi(this);

    connect(ui->pBApply, SIGNAL(clicked(bool)), this, SLOT(updateLayout()));

    layBoxes = new QHBoxLayout;
    layPlots = new QVBoxLayout;

    boxes.resize(10);
    plots.resize(10);

    QStringList menus;
    menus <<"Raw Acc." <<"Raw Gyro" <<"Raw Mag." <<"Cal. Acc." <<"Cal Gyro" <<"Cal. Mag."
         <<"ENU Acc" <<"ENU KF Acc" <<"Euler angles" <<"Velocity" <<"Position" <<"Neural output";

    labels.clear();
    labels << "Raw Acceleration [g]"
           << "Raw Rotation rate [deg/s]"
           << "Raw Magnetic field [Gauss]"
           << "Calibrated Acceleration [g]"
           << "Calibrated Rotation rate [deg/s]"
           << "Calibrated Magnetic field [mGauss]"
           << "ENU Acceleration [m/s^2]"
           << "ENU Acceleration after KF [m/s^2]"
           << "Euler Angles [deg]"
           << "Velocity [m/s]"
           << "Position [m]"
           << "State and Neural Network output";

    for(int i = 0; i<10; i++)
    {
        boxes[i] = new QComboBox(this);
        layBoxes->addWidget(boxes[i]);
        for(int j = 0; j<menus.size(); j++) boxes[i]->addItem(menus[j], j);

        plots[i] = new PPlotGroup("plot"+QString::number(i), this);
        layPlots->addWidget(plots[i]);
        plots[i]->hide();
        plots[i]->showBounds();
    }

    layBoxes->addStretch();
    ui->scrollBoxesCont->setLayout(layBoxes);
    ui->scrollPlotsCont->setLayout(layPlots);

    boxes[0]->setCurrentText("Cal. Acc.");
    plots[0]->setRangeY(-2, 2);
    boxes[1]->setCurrentText("Neural output");
    setVisibleThings(ui->spinBox->value());
}

CustomTab::~CustomTab()
{
    delete ui;
}

void CustomTab::addNewData(QVector<QVector3D> &in, float time, float history)
{
    int count = ui->spinBox->value();

    for(int i = 0; i<count; i++)
    {
        plots[i]->addData(time, in[boxes[i]->currentData().toInt()]);
        plots[i]->setRangeX(time-history, time);
    }
}

void CustomTab::updatePlots()
{
    int count = ui->spinBox->value();

    for(int i = 0; i<count; i++)
    {
        plots[i]->replot();
    }
}

void CustomTab::cleanUpPlots()
{
    int count = ui->spinBox->value();

    for(int i = 0; i<count; i++)
    {
        plots[i]->cleanUp();
    }
}

void CustomTab::on_spinBox_valueChanged(int arg1)
{
    updateLayout();
}

void CustomTab::setVisibleThings(int num)
{
    updateLayout();
}

void CustomTab::updateLayout()
{
    int num = ui->spinBox->value();
    for(int i = 0; i<10; i++)
    {
        if(i<num)
        {
            boxes[i]->show();
            plots[i]->show();
            plots[i]->setTitle(labels[boxes[i]->currentIndex()]);
        }
        else
        {
            boxes[i]->hide();
            plots[i]->hide();
        }
    }
}

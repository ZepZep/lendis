#ifndef PACKFORMATER_H
#define PACKFORMATER_H

#include <QDialog>
#include <QVector3D>
#include <QListWidgetItem>
#include <QSettings>
#include <QFile>

#include <QDebug>

namespace Ui {
class packFormater;
}

class packFormater : public QDialog
{
    Q_OBJECT

public:
    explicit packFormater(QWidget *parent = 0);
    ~packFormater();

    QString formatFrom(QVector<QVector3D> in1, QVector<double> in2);

private:
    Ui::packFormater *ui;

    QVector<QListWidgetItem *> items;
    QStringList format;
    QVector<int> starts;
    int vectorCount;

    QSettings *presets;
    QFile *file;
    bool pause;

    void closeEvent(QCloseEvent *);
    void loadSettings();
    void saveSettings();

private slots:
    void updateFormat();
    void moveLeft();
    void moveRight();

    void on_pBSave_clicked();

public slots:
    void write(QVector<QVector3D> in1, QVector<double> in2);
    void startFileWrite(QString path);
    void stopFileWrite(bool hard);

    void sendPackFromString(QString in, float frameTime, float wholeTime, int where, int &state);

signals:
    int sendToCal(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, bool button);
};

#endif // PACKFORMATER_H

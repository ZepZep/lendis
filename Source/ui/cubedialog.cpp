#include "cubedialog.h"
#include "ui_cubedialog.h"

CubeDialog::CubeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CubeDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Cube View");

    presets = new QSettings("config.ini", QSettings::IniFormat);
    avgSPS = new Avrg(30);
    loadSettings();
    reset();

    //can be better
    QVBoxLayout * layout = new QVBoxLayout(ui->alignWidget);
    glWidget = new glCubeWidget(ui->alignWidget);
    glWidget->setMinimumSize(QSize(10, 10));
    glWidget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    layout->addWidget(glWidget);

    glWidget->updateVectors(QQuaternion(1, 0, 0, 0).normalized(), QVector3D(0, 0, 0));
    ui->sBViewDist->setValue(100);
    glWidget->setViewDistance(ui->sBViewDist->value());
}

CubeDialog::~CubeDialog()
{
    delete ui;
}

void CubeDialog::loadSettings()
{
    presets->beginGroup("glView");
    ui->chBLockPos->setChecked(presets->value("lockPos", false).toBool());
    this->setGeometry(presets->value("geometry", this->geometry()).toRect());
    presets->endGroup();
}

void CubeDialog::closeEvent(QCloseEvent *event)
{
    presets->beginGroup("glView");
    presets->setValue("lockPos", ui->chBLockPos->isChecked());
    presets->setValue("geometry", this->geometry());
    presets->endGroup();
    presets->sync();

    event->accept();
}

void CubeDialog::addDerotData(QVector3D velocity, QVector3D position, QVector3D acc, QVector3D kalAcc, QQuaternion orientation, float frameTime, float wholeTime)
{
    if(ui->chBLockPos->isChecked()) position = QVector3D(0, 0, 0);
    glWidget->updateVectors(orientation, position);

    avgSPS->getrAvr(1/frameTime);

    lVel = velocity;
    lPos = position;
    lAcc = acc;
    lOrientation = orientation;
    wTime = wholeTime;
}

void CubeDialog::updateAll()
{
    glWidget->callUpdate();
    ui->lSPS->setText("SPS: "+QString::number((int)avgSPS->getrAvr(83)));

    float mez = 0.001;
    if(abs(lPos.x())<mez) lPos.setX(0);
    if(abs(lPos.y())<mez) lPos.setY(0);
    if(abs(lPos.z())<mez) lPos.setZ(0);

    if(abs(lVel.x())<mez) lVel.setX(0);
    if(abs(lVel.y())<mez) lVel.setY(0);
    if(abs(lVel.z())<mez) lVel.setZ(0);

    ui->lPosX->setText(QString::number(lPos.x(), 'g', 3));
    ui->lPosY->setText(QString::number(lPos.y(), 'g', 3));
    ui->lPosZ->setText(QString::number(lPos.z(), 'g', 3));

    ui->lVelX->setText(QString::number(lVel.x(), 'g', 3));
    ui->lVelY->setText(QString::number(lVel.y(), 'g', 3));
    ui->lVelZ->setText(QString::number(lVel.z(), 'g', 3));

    ui->lQuatW->setText(QString::number(lOrientation.scalar(), 'f', 3));
    ui->lQuatX->setText(QString::number(lOrientation.x(), 'f', 3));
    ui->lQuatY->setText(QString::number(lOrientation.y(), 'f', 3));
    ui->lQuatZ->setText(QString::number(lOrientation.z(), 'f', 3));
}

void CubeDialog::reset()
{
    lVel = QVector3D(0,0,0);
    lPos = QVector3D(0,0,0);
    lAcc = QVector3D(0,0,0);
    lOrientation = QQuaternion(1,0,0,0);
}

void CubeDialog::on_pBResView_clicked()
{
    glWidget->resetView();
}

void CubeDialog::on_sBViewDist_valueChanged(double arg1)
{
    glWidget->setViewDistance(arg1);
}

void CubeDialog::on_pBClose_clicked()
{
    this->close();
}

void CubeDialog::on_pBAutoView_clicked()
{
    glWidget->autoView();
}

void CubeDialog::on_pBResAll_clicked()
{
    emit sigReset();
}

void CubeDialog::on_pBCalibrate_clicked()
{
    emit sigCalibrate();
}

#ifndef CUSTOMTAB_H
#define CUSTOMTAB_H

#include <QWidget>
#include <QVector3D>
#include <QComboBox>

#include "utils/pplotgroup.h"

namespace Ui {
class CustomTab;
}

class CustomTab : public QWidget
{
    Q_OBJECT

public:
    explicit CustomTab(QWidget *parent = 0);
    ~CustomTab();

    void addNewData(QVector<QVector3D> &in, float time, float history);
    void updatePlots();
    void cleanUpPlots();

private slots:
    void on_spinBox_valueChanged(int arg1);

private:
    Ui::CustomTab *ui;

    QHBoxLayout* layBoxes;
    QVBoxLayout* layPlots;

    QVector<QComboBox *> boxes;
    QVector<PPlotGroup *> plots;

    QStringList labels;

    void setVisibleThings(int num);

private slots:
    void updateLayout();
};
#endif // CUSTOMTAB_H

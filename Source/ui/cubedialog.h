#ifndef CUBEDIALOG_H
#define CUBEDIALOG_H

#include <QDialog>
#include <QQuaternion>
#include <QVector3D>
#include <QSettings>
#include <math.h>

#include "visuals/glcubewidget.h"
#include "utils/putils.h"

namespace Ui {
class CubeDialog;
}

class CubeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CubeDialog(QWidget *parent = 0);
    ~CubeDialog();

protected:
    glCubeWidget* glWidget;
    Avrg *avgSPS;
    QSettings *presets;

    float wTime;

    QVector3D lAcc, lVel, lPos;
    QQuaternion lOrientation;

private:
    Ui::CubeDialog *ui;
    void loadSettings();
    void closeEvent(QCloseEvent *event);
signals:
    void sigReset();
    void sigCalibrate();
public slots:
    void addDerotData(QVector3D velocity, QVector3D position, QVector3D acc, QVector3D kalAcc, QQuaternion orientation, float frameTime, float wholeTime);
    void updateAll();
    void reset();
private slots:
    void on_pBResView_clicked();
    void on_sBViewDist_valueChanged(double arg1);
    void on_pBClose_clicked();
    void on_pBAutoView_clicked();
    void on_pBResAll_clicked();
    void on_pBCalibrate_clicked();
};

#endif // CUBEDIALOG_H

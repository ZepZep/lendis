#ifndef VIZUALIZER_H
#define VIZUALIZER_H

#include <QDebug>

#include <QMainWindow>
#include <QDialog>
#include <QVector3D>
#include <QTimer>
#include <QQuaternion>
#include <QSettings>

#include "utils/pplotgroup.h"
#include "utils/putils.h"
#include "ui/customtab.h"
#include "visuals/tcompass.h"
#include "dealers/filehandler.h"

namespace Ui {
class Vizualizer;
}

class Vizualizer : public QMainWindow
{
    Q_OBJECT

public:
    explicit Vizualizer(QWidget *parent = 0);
    ~Vizualizer();
    void reconnect();

    FileHandler *fileHand;
    TCompass *compass;
    QDialog *fDialog;

private:
    Ui::Vizualizer *ui;
    void loadSettings();
    void closeEvent(QCloseEvent *event);

    QSettings *presets;
    PPlotGroup *pgRawAcc, *pgRawGyr, *pgRawMag;
    PPlotGroup *pgCalAcc, *pgCalGyr, *pgCalMag;
    PPlotGroup *pgRotAcc, *pgRotAngles;
    PPlotGroup *pgVelocity, *pgPosition;
    PPlotGroup *pgAcc, *pgKalAcc;
    CustomTab *cusTab;

    float history;
    int defFPS;

    QTimer *timerReplot;
    QTimer *disTimer;
    QGraphicsScene *sceneState;

    bool fileWriting;
    int fState;
    int dataWritten;

    QVector<QVector3D> lastVectors;
    QVector<double> lastNumbers;

    bool frozen;
    bool filePopped;

    QVBoxLayout *fDialogLay;

signals:
    void sigConnect(QString address);
    void sigDisconnect();
    void sigReset();
    void sigCalibrate();
    void sigCubeShow();
    void sigRecShow();
    void sigSettingsShow();
    void sigFormatShow();
    void sigFileStart(QString path);
    void sigFileStop(bool hard);
    void sigWriteFormated(QVector<QVector3D> in1, QVector<double> in2);
    void sigUpdateAll();
    void sigClosing();

public slots:
    void addRawData(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, bool button);
    void addCalData(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, int mod, QVector3D brainStatus);
    void addIntegData(QVector3D velocity, QVector3D position, QVector3D acc, QVector3D kalAcc, QQuaternion orientation, float frameTime, float wholeTime);
    void restart();

    void setSending(bool value);

private slots:
    void timerReplEnd();

    void on_pBConnect_clicked();
    void on_pBDisconnect_clicked();
    void on_lineEditPort_returnPressed();

    void on_pbReset_clicked();
    void on_pbCalibrate_clicked();
    void on_pb3DView_clicked();
    void on_pbSettings_clicked();
    void on_pBHide_clicked();

    void on_sBTarFPS_valueChanged(int arg1);
    void on_pBFreeze_clicked();
    void on_pushButton_clicked();
    void on_pBFilePopup_clicked();
};

#endif // VIZUALIZER_H

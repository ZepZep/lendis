#include "settings.h"
#include "ui_settings.h"

#include <QDebug>

Settings::Settings(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);

    this->setWindowTitle("Settings");
    presets = new QSettings("config.ini", QSettings::IniFormat);

    connect(ui->lEKalP, SIGNAL(returnPressed()), this, SLOT(on_pBSave_clicked()));
    connect(ui->lEKalQ, SIGNAL(returnPressed()), this, SLOT(on_pBSave_clicked()));
    connect(ui->lEKalR, SIGNAL(returnPressed()), this, SLOT(on_pBSave_clicked()));

    dir = new QDir(QDir::current().path()+"/NNs");

    this->setGeometry(presets->value("settings/geometry", this->geometry()).toRect());

    loadNNs();
    loadSettings();
}

Settings::~Settings()
{
    delete ui;
}

void Settings::loadSettings()
{
    presets->beginGroup("integrator");
    ui->lEKalP->setText(QString::number(presets->value("p", 1).toDouble()));
    ui->lEKalQ->setText(QString::number(presets->value("q", 0.1).toDouble()));
    ui->lEKalR->setText(QString::number(presets->value("r", 1).toDouble()));
    presets->endGroup();

    presets->beginGroup("neural");
    ui->sBNNBoundary->setValue(presets->value("boundary", -0.95).toDouble());
    ui->nnSelector->setCurrentIndex(presets->value("nnInUse", 0).toInt());
    ui->lEKFNNP->setText(QString::number(presets->value("p", 0.1).toDouble()));
    ui->lEKFNNQ->setText(QString::number(presets->value("q", 0.5).toDouble()));
    ui->lEKFNNR->setText(QString::number(presets->value("r", 25).toDouble()));
    ui->groupBoxSimpleDet->setChecked(presets->value("useSimple", true).toBool());
    ui->groupBoxNN->setChecked(presets->value("useNN", true).toBool());
    presets->endGroup();

    presets->beginGroup("calibrator");
    ui->lEAccLim->setText(QString::number(presets->value("tolAcc1", 0.1).toDouble()));
    ui->lEGyrLim->setText(QString::number(presets->value("tolGyr1", 1.5).toDouble()));
    ui->lEStillLimit->setText(QString::number(presets->value("stillNeed", 4).toDouble()));
    presets->endGroup();

    presets->beginGroup("settings");
    this->setGeometry(presets->value("geometry", this->geometry()).toRect());
    ui->chBOnlyGyro->setChecked(presets->value("gyroOnly", false).toBool());
    ui->chBResButton->setChecked(presets->value("buttonReset", false).toBool());
    presets->endGroup();
}

void Settings::loadNNs()
{
    QStringList filters;
    filters << "TDNN*" <<"TS*";
    QStringList entries = dir->entryList(filters);

    ui->nnSelector->clear();
    ui->nnSelector->addItems(entries);
}

void Settings::autochooseNN()
{

    emit nnChanged(dir->path()+"/"+ui->nnSelector->currentText());
}

void Settings::closeEvent(QCloseEvent *)
{
    presets->setValue("settings/geometry", this->geometry());
    presets->sync();
}

void Settings::on_pBSave_clicked()
{
    presets->beginGroup("integrator");
    presets->setValue("p", ui->lEKalP->text().toDouble());
    presets->setValue("q", ui->lEKalQ->text().toDouble());
    presets->setValue("r", ui->lEKalR->text().toDouble());
    presets->endGroup();

    presets->beginGroup("neural");
    presets->setValue("boundary", ui->sBNNBoundary->value());
    presets->setValue("nnInUse", ui->nnSelector->currentIndex());
    presets->setValue("p", ui->lEKFNNP->text().toDouble());
    presets->setValue("q", ui->lEKFNNQ->text().toDouble());
    presets->setValue("r", ui->lEKFNNR->text().toDouble());
    presets->setValue("useSimple", ui->groupBoxSimpleDet->isChecked());
    presets->setValue("useNN", ui->groupBoxNN->isChecked());
    presets->endGroup();

    presets->beginGroup("calibrator");
    presets->setValue("tolAcc1", ui->lEAccLim->text().toDouble());
    presets->setValue("tolGyr1", ui->lEGyrLim->text().toDouble());
    presets->setValue("stillNeed", ui->lEStillLimit->text().toInt());
    presets->endGroup();

    presets->beginGroup("settings");
    presets->setValue("gyroOnly", ui->chBOnlyGyro->isChecked());
    presets->setValue("buttonReset", ui->chBResButton->isChecked());
    presets->endGroup();

    presets->sync();
    emit save();
}

void Settings::on_nnSelector_currentIndexChanged(const QString &arg1)
{
    emit nnChanged(dir->path()+"/"+arg1);
}


void Settings::on_groupBoxSimpleDet_toggled(bool arg1)
{
    if(!arg1)
    {
        if(!ui->groupBoxNN->isChecked())
            ui->groupBoxNN->setChecked(true);
    }
}

void Settings::on_groupBoxNN_toggled(bool arg1)
{
    if(!arg1)
    {
        if(!ui->groupBoxSimpleDet->isChecked())
            ui->groupBoxSimpleDet->setChecked(true);
    }
}

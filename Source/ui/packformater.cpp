#include "packformater.h"
#include "ui_packformater.h"

packFormater::packFormater(QWidget *parent)  :
    QDialog(parent),
    ui(new Ui::packFormater)
{
    ui->setupUi(this);

    this->setWindowTitle("Select format");

    connect(ui->pBLeft, SIGNAL(clicked(bool)), this, SLOT(moveLeft()));
    connect(ui->pBRight, SIGNAL(clicked(bool)), this, SLOT(moveRight()));

    presets = new QSettings("config.ini", QSettings::IniFormat);
    file = new QFile(this);

    ui->listWidgetDisabled->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->listWidgetDisabled->setDragEnabled(true);
    ui->listWidgetDisabled->viewport()->setAcceptDrops(true);
    ui->listWidgetDisabled->setDropIndicatorShown(true);
    ui->listWidgetDisabled->setDragDropMode(QAbstractItemView::InternalMove);

    ui->listWidgetEnabled->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->listWidgetEnabled->setDragEnabled(true);
    ui->listWidgetEnabled->viewport()->setAcceptDrops(true);
    ui->listWidgetEnabled->setDropIndicatorShown(true);
    ui->listWidgetEnabled->setDragDropMode(QAbstractItemView::InternalMove);

    QStringList labels;
    //    labels <<"AccRaw" <<"AccCal" <<"AccDer" <<"GyrRaw" <<"GyrCal" <<"Euler" <<"MagRaw" <<"MagCal" <<"NeuralNet"
    //          <<"FrameTime" <<"ElapTime" <<"Button";
    labels <<"Raw Acc." <<"Raw Gyro" <<"Raw Mag." <<"Cal. Acc." <<"Cal Gyro" <<"Cal. Mag."
          <<"ECI Acc" <<"ECI KF Acc" <<"Euler angles" <<"Velocity" <<"Position" <<"Neural output"
         <<"FrameTime" <<"ElapTime" <<"Button";
    vectorCount = 12;

    items.resize(labels.size());
    for(int i = 0; i<labels.size(); i++)
    {
        items[i] = new QListWidgetItem(labels[i], ui->listWidgetDisabled);
        items[i]->setToolTip(QString::number(i));
    }

    loadSettings();
}

packFormater::~packFormater()
{
    delete ui;
}

QString packFormater::formatFrom(QVector<QVector3D> in1, QVector<double> in2)
{
    QString output;

    int count = format.size();

    for(int i=0; i<count; i++)
    {
        int index = format[i].toInt();
        if(index < vectorCount)
        {
            output += QString::number(in1[index].x());
            output += " ";
            output += QString::number(in1[index].y());
            output += " ";
            output += QString::number(in1[index].z());
            output += " ";
        }
        else
            output += QString::number(in2[index-vectorCount]) + " ";
    }
    output.chop(1);

    return output;
}

void packFormater::closeEvent(QCloseEvent *)
{
    saveSettings();
}

void packFormater::loadSettings()
{
    QStringList en;
    presets->beginGroup("formater");
    en = presets->value("enabled", "").toString().split(" ", QString::SkipEmptyParts);
    presets->endGroup();

    for(int i = 0; i<en.size(); i++)
    {
        ui->listWidgetDisabled->setCurrentItem(items[((QString)en[i]).toInt()]);
        moveLeft();
    }
}

void packFormater::saveSettings()
{
    updateFormat();
    QString en = format.join(' ');

    presets->beginGroup("formater");
    presets->setValue("enabled", en);
    presets->endGroup();
}

void packFormater::updateFormat()
{
    int pos = 0;
    format.clear();
    starts.clear();
    int count = ui->listWidgetEnabled->count();
    for(int i=0; i<count; i++)
    {
        QString it = ui->listWidgetEnabled->item(i)->toolTip();
        format << it;
        starts << pos;
        if(it.toInt() < vectorCount) pos += 3;
        else pos += 1;
    }
}

void packFormater::moveLeft()
{
    QListWidgetItem * current = ui->listWidgetDisabled->takeItem(ui->listWidgetDisabled->currentRow());
    ui->listWidgetEnabled->addItem(current);
    updateFormat();
}

void packFormater::moveRight()
{
    QListWidgetItem * current = ui->listWidgetEnabled->takeItem(ui->listWidgetEnabled->currentRow());
    ui->listWidgetDisabled->addItem(current);
    updateFormat();
}

void packFormater::write(QVector<QVector3D> in1, QVector<double> in2)
{
    QString toWrite = formatFrom(in1, in2);

    QTextStream out(file);
    out << toWrite <<"\n";
}

void packFormater::startFileWrite(QString path)
{
    if(path == "")
    {
        pause = 0;
    }
    else
    {
        file->setFileName(path);
        file->open(QIODevice::WriteOnly);
    }
}

void packFormater::stopFileWrite(bool hard)
{
    if(hard)
    {
        if(file->isOpen()) file->close();
        pause = 0;
    }
    else
    {
        pause = 1;
    }
}

void packFormater::sendPackFromString(QString in, float frameTime, float wholeTime, int where, int &state)
{
    QVector3D acc(0,0,0), gyr(0,0,0), mag(0,0,0);
    bool button = 0;
    QStringList list = in.split(" ");

    if(where == 1)
    {
        for(int i = 0; i<format.size(); i++)
        {
            if(i>=list.size())
            {
                state = 1;
                return;
            }
            if(format[i] == "0")
            {
                acc.setX(((QString)list[starts[i]]).toFloat());
                acc.setY(((QString)list[starts[i]+1]).toFloat());
                acc.setZ(((QString)list[starts[i]+2]).toFloat());
            }
            else if(format[i] == "1")
            {
                gyr.setX(((QString)list[starts[i]]).toFloat());
                gyr.setY(((QString)list[starts[i]+1]).toFloat());
                gyr.setZ(((QString)list[starts[i]+2]).toFloat());
            }
            else if(format[i] == "2")
            {
                mag.setX(((QString)list[starts[i]]).toFloat());
                mag.setY(((QString)list[starts[i]+1]).toFloat());
                mag.setZ(((QString)list[starts[i]+2]).toFloat());
            }
            else if(format[i] == "12")
            {
                frameTime = ((QString)list[starts[i]]).toFloat();
            }
            else if(format[i] == "13")
            {
                wholeTime = ((QString)list[starts[i]]).toFloat();
            }
        }
        emit sendToCal(acc, gyr, mag, frameTime, wholeTime, button);
    }
    state = 0;
}

void packFormater::on_pBSave_clicked()
{
    updateFormat();
    saveSettings();
}

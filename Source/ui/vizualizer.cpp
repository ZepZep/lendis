#include "vizualizer.h"
#include "ui_vizualizer.h"

Vizualizer::Vizualizer(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::Vizualizer)
{
    ui->setupUi(this);

    presets = new QSettings("config.ini", QSettings::IniFormat);
    loadSettings();

    history = 10;

    timerReplot = new QTimer(this);
    disTimer = new QTimer(this);
    disTimer->setSingleShot(true);

    fileHand = new FileHandler(this);
    ui->layFiles->addWidget(fileHand);
    compass = new TCompass(this);
    ui->vLayComp->addWidget(compass);

    fDialog = new QDialog();
    fDialogLay = new QVBoxLayout();
    fDialog->setLayout(fDialogLay);
    fDialog->setWindowTitle("File Handeling");
    filePopped = false;

    connect(timerReplot, SIGNAL(timeout()), this, SLOT(timerReplEnd()));
    connect(disTimer, SIGNAL(timeout()), this, SLOT(on_pBDisconnect_clicked()));

    lastVectors.resize(12);
    lastNumbers.resize(3);

    //RawData
    pgRawAcc = new PPlotGroup("Raw Acceleration [g]", this);
    pgRawGyr = new PPlotGroup("Raw Rotation rate [deg/s]", this);
    pgRawMag = new PPlotGroup("Raw Magnetic field [Gauss]", this);
    QVBoxLayout *layRaw = new QVBoxLayout;
    layRaw->addWidget(pgRawAcc);
    layRaw->addWidget(pgRawGyr);
    layRaw->addWidget(pgRawMag);
    ui->scrollRawCont->setLayout(layRaw);
    pgRawAcc->setRangeY(-2, 2);
    pgRawGyr->setRangeY(-500, 500);
    pgRawMag->setRangeY(-2, 2);

    //Calibrated data
    pgCalAcc = new PPlotGroup("Calibrated Acceleration [g]", this);
    pgCalGyr = new PPlotGroup("Calibrated Rotation rate [deg/s]", this);
    pgCalMag = new PPlotGroup("Calibrated Magnetic field [mGauss]", this);
    QVBoxLayout *layCal = new QVBoxLayout;
    layCal->addWidget(pgCalAcc);
    layCal->addWidget(pgCalGyr);
    layCal->addWidget(pgCalMag);
    ui->scrollCalCont->setLayout(layCal);
    pgCalAcc->setRangeY(-2, 2);
    pgCalGyr->setRangeY(-550, 550);
    pgCalMag->setRangeY(-500, 500);

    //Derotated
    pgRotAcc = new PPlotGroup("ENU Acceleration [m/s^2]", this);
    pgRotAngles = new PPlotGroup("Euler Angles [deg]", this);
    QVBoxLayout *layDerot = new QVBoxLayout;
    layDerot->addWidget(pgRotAcc);
    layDerot->addWidget(pgRotAngles);
    ui->scrollDerCont->setLayout(layDerot);
    pgRotAcc->setRangeY(-10, 10);
    pgRotAngles->setRangeY(-185, 185);

    //Kalman
    pgAcc = new PPlotGroup("ENU Acceleration [m/s^2]", this);
    pgKalAcc = new PPlotGroup("ENU Acceleration after KF [m/s^2]", this);
    QVBoxLayout *layKalF = new QVBoxLayout;
    layKalF->addWidget(pgAcc);
    layKalF->addWidget(pgKalAcc);
    ui->scrollKalCont->setLayout(layKalF);
    pgAcc->setRangeY(-10, 10);
    pgKalAcc->setRangeY(-10, 10);

    //Integrated Data
    pgVelocity = new PPlotGroup("Velocity [m/s]", this);
    pgPosition = new PPlotGroup("Position [m]", this);
    QVBoxLayout *layInteg = new QVBoxLayout;
    layInteg->addWidget(pgVelocity);
    layInteg->addWidget(pgPosition);
    ui->scrollIntCont->setLayout(layInteg);
    pgVelocity->setRangeY(-2, 2);
    pgPosition->setRangeY(-1, 1);
    pgVelocity->showBounds();
    pgPosition->showBounds();

    //Custom
    cusTab = new CustomTab(this);
    QVBoxLayout *layCus = new QVBoxLayout;
    layCus->addWidget(cusTab);
    ui->pCustom->setLayout(layCus);

    //Global
    fileWriting=false;
    fState = 0;
    dataWritten = 0;
    frozen = false;
    ui->sBTarFPS->setValue(defFPS);
    ui->sBTarFPS->valueChanged(defFPS);
}

Vizualizer::~Vizualizer()
{
    delete ui;
}

void Vizualizer::closeEvent(QCloseEvent *event)
{
    presets->beginGroup("vizualizer");
    presets->setValue("defFPS", defFPS);
    presets->setValue("history", history);
    presets->setValue("port", ui->lineEditPort->text());
    presets->setValue("tabIndex", ui->toolBox->currentIndex());
    //    presets->setValue("filePath", ui->labelPath->toolTip());
    //    presets->setValue("fileName", ui->lineEditFileName->text());
    presets->setValue("geometry", this->geometry());
    presets->endGroup();
    presets->sync();
    emit sigClosing();

    event->accept();
}

void Vizualizer::loadSettings()
{
    presets->beginGroup("vizualizer");
    defFPS = presets->value("defFPS", 25).toInt();
    history = presets->value("history", 15).toFloat();
    ui->toolBox->setCurrentIndex(presets->value("tabIndex", 0).toInt());
    ui->lineEditPort->setText(presets->value("port", "ttyACM0").toString());
    QString filePath = presets->value("filePath", QDir::home().path()).toString();
    //    ui->labelPath->setText(ui->labelPath->fontMetrics().elidedText(filePath, Qt::ElideMiddle, 200));
    //    ui->labelPath->setToolTip(filePath);
    //    ui->lineEditFileName->setText(presets->value("fileName", "output.txt").toString());
    this->setGeometry(presets->value("geometry", this->geometry()).toRect());
    presets->endGroup();
}

void Vizualizer::reconnect()
{
    on_pBConnect_clicked();
}

void Vizualizer::restart()
{
    emit sigReset();
}

void Vizualizer::setSending(bool value)
{
    fileWriting = value;
}

//------------------------------------INCOMMING DATA-----------------------------------------------
void Vizualizer::addRawData(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, bool button)
{
    if(fileWriting)
    {
        dataWritten++;
        emit sigWriteFormated(lastVectors, lastNumbers);
    }
    cusTab->addNewData(lastVectors, lastNumbers[1], history);

    lastVectors[0] = acc;
    lastVectors[1] = gyr;
    lastVectors[2] = mag;

    lastNumbers[0] = frameTime;
    lastNumbers[1] = wholeTime;
    if(button) lastNumbers[2] = 1;
    else lastNumbers[2] = 0;

    pgRawAcc->addData(wholeTime, acc);
    pgRawGyr->addData(wholeTime, gyr);
    pgRawMag->addData(wholeTime, mag);

    pgRawAcc->setRangeX(wholeTime-history, wholeTime);
    pgRawGyr->setRangeX(wholeTime-history, wholeTime);
    pgRawMag->setRangeX(wholeTime-history, wholeTime);
}

void Vizualizer::addCalData(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, int mod, QVector3D brainStatus)
{
    lastVectors[3] = acc;
    lastVectors[4] = gyr;
    lastVectors[5] = mag;
    lastVectors[11] = brainStatus;

    pgCalAcc->addData(wholeTime, acc);
    pgCalGyr->addData(wholeTime, gyr);
    pgCalMag->addData(wholeTime, mag);

    pgCalAcc->setRangeX(wholeTime-history, wholeTime);
    pgCalGyr->setRangeX(wholeTime-history, wholeTime);
    pgCalMag->setRangeX(wholeTime-history, wholeTime);
}

void Vizualizer::addIntegData(QVector3D velocity, QVector3D position, QVector3D acc, QVector3D kalAcc, QQuaternion orientation, float frameTime, float wholeTime)
{
    QVector3D euler = eulerFromQuat(orientation);
    euler *= 180/M_PI;

    //signals

    lastVectors[6] = acc;       //derotated acc
    lastVectors[7] = kalAcc;    //derotated acc
    lastVectors[8] = euler;     //euler angles
    lastVectors[9] = velocity;  //derotated acc
    lastVectors[10] = position; //derotated acc

    compass->setHeading(euler.z());

    pgRotAcc->addData(wholeTime, acc);
    pgRotAngles->addData(wholeTime, euler);
    pgVelocity->addData(wholeTime, velocity);
    pgPosition->addData(wholeTime, position);
    pgAcc->addData(wholeTime, acc);
    pgKalAcc->addData(wholeTime, kalAcc);

    pgRotAcc->setRangeX(wholeTime-history, wholeTime);
    pgRotAngles->setRangeX(wholeTime-history, wholeTime);
    pgVelocity->setRangeX(wholeTime-history, wholeTime);
    pgPosition->setRangeX(wholeTime-history, wholeTime);
    pgAcc->setRangeX(wholeTime-history, wholeTime);
    pgKalAcc->setRangeX(wholeTime-history, wholeTime);
}

//--------------------------PLOT UPDATE-----------------------------
void Vizualizer::on_sBTarFPS_valueChanged(int arg1)
{
    defFPS = arg1;
    timerReplot->start(1000/arg1);
}

void Vizualizer::timerReplEnd()
{
    if(fileWriting) fileHand->setSamplesWritten(dataWritten);

    pgRawAcc->cleanUp();
    pgRawGyr->cleanUp();
    pgRawMag->cleanUp();

    pgCalAcc->cleanUp();
    pgCalGyr->cleanUp();
    pgCalMag->cleanUp();

    pgRotAcc->cleanUp();
    pgRotAngles->cleanUp();

    pgPosition->cleanUp();
    pgVelocity->cleanUp();

    pgAcc->cleanUp();
    pgKalAcc->cleanUp();

    cusTab->cleanUpPlots();

    if(!frozen)
    {
        if(ui->toolBox->currentIndex()==0)
        {
            pgRawAcc->replot();
            pgRawGyr->replot();
            pgRawMag->replot();
        }
        else if(ui->toolBox->currentIndex()==1)
        {
            pgCalAcc->replot();
            pgCalGyr->replot();
            pgCalMag->replot();
        }
        else if(ui->toolBox->currentIndex()==2)
        {
            pgRotAcc->replot();
            pgRotAngles->replot();
        }
        else if(ui->toolBox->currentIndex()==3)
        {
            pgAcc->replot();
            pgKalAcc->replot();
        }
        else if(ui->toolBox->currentIndex()==4)
        {
            pgPosition->replot();
            pgVelocity->replot();
        }
        else if(ui->toolBox->currentIndex()==5)
        {
            cusTab->updatePlots();
        }

        compass->updateAll();
        emit sigUpdateAll();
    }
}

//---------------------------SERIAL PORT--------------------------------
void Vizualizer::on_pBConnect_clicked()
{
    emit sigConnect(ui->lineEditPort->text());
}

void Vizualizer::on_pBDisconnect_clicked()
{
    emit sigDisconnect();
}

void Vizualizer::on_lineEditPort_returnPressed()
{
    emit sigConnect(ui->lineEditPort->text());
}

//------------------------------BUTTONS----------------------------------
//connect(ui->pbReset, SIGNAL(clicked()), this, SIGNAL(sigReset()));
void Vizualizer::on_pbReset_clicked()
{
    emit sigReset();
}

void Vizualizer::on_pbCalibrate_clicked()
{
    emit sigCalibrate();
}

void Vizualizer::on_pb3DView_clicked()
{
    emit sigCubeShow();
}

void Vizualizer::on_pbSettings_clicked()
{
    emit sigSettingsShow();
}

void Vizualizer::on_pBHide_clicked()
{
    if(ui->toolBox->isVisible())ui->toolBox->hide();
    else ui->toolBox->show();
}

void Vizualizer::on_pBFreeze_clicked()
{
    if(frozen)
    {
        ui->pBFreeze->setText("Freeze");
        frozen = false;
    }
    else
    {
        ui->pBFreeze->setText("Unfreeze");
        frozen = true;
    }
}

void Vizualizer::on_pushButton_clicked()
{
    disTimer->start(ui->lineEdit->text().toInt());
}

void Vizualizer::on_pBFilePopup_clicked()
{
    if(filePopped)
    {
        if(fDialog->isVisible())
        {
            filePopped = false;
            fDialogLay->removeWidget(fileHand);
            ui->layFiles->addWidget(fileHand);
            fDialog->close();
        }
        else fDialog->show();
    }
    else
    {
        filePopped = true;
        ui->layFiles->removeWidget(fileHand);
        fDialogLay->addWidget(fileHand);
        fDialog->show();
    }
}

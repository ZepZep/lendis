#include "integrator.h"

Integrator::Integrator(QObject *parent) : QObject(parent)
{
    kalAcc = new Kalman(0,0,0,20);
    presets = new QSettings("config.ini", QSettings::IniFormat);
    reset();
}

Integrator::~Integrator()
{

}

void Integrator::reset()
{
    pos = QVector3D(0,0,0);
    vel = QVector3D(0,0,0);
    loadSettings();
}

void Integrator::loadSettings()
{
    float p, q, r;
    presets->beginGroup("integrator");
    p = presets->value("p", 1).toFloat();
    q = presets->value("q", 0.1).toFloat();
    r = presets->value("r", 1).toFloat();
    presets->endGroup();

    kalAcc->reset(p, q, r, 20);
    num = 0;
    avga = QVector3D(0,0,0);
    avgv = QVector3D(0,0,0);
}

void Integrator::derotDataRecieve(QVector3D acc, QQuaternion orientation, float frameTime, float wholeTime, int mod, QVector3D BrainStatus)
{
    if(mod == 1) vel = QVector3D(0,0,0);
    //if(mod == 1) acc = QVector3D(0,0,1);
    acc.setZ(acc.z()-1);
    acc *= 9.81;

    if(BrainStatus.y() < -0.95) vel = QVector3D(0,0,0);

    QVector3D accKal = kalAcc->getrValue(acc);

    //s = v0 t + 1/2 a t^2
    QVector3D a = acc;
       // num+=1;
//        avga = ((num-1)*avga+a)/num;
//        a=a-avga*0.3;
    pos += vel*frameTime + frameTime*frameTime*0.5*a;
    vel += a*frameTime;
//        avgv = ((num-1)*avgv+vel)/num;
//        vel=vel-avgv;
    //qDebug() << vel << avgv;
    emit addFinalData(vel, pos, acc, accKal, orientation, frameTime, wholeTime);
}

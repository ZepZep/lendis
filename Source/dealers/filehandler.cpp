#include "filehandler.h"
#include "ui_filehandler.h"

FileHandler::FileHandler(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FileHandler)
{
    ui->setupUi(this);

    setMaximumWidth(500);

    presets = new QSettings("config.ini", QSettings::IniFormat);
    fin = new QFile(this);
    inTimer = new QTimer(this);
    inTimer->setSingleShot(true);
    connect(inTimer, SIGNAL(timeout()), this, SLOT(sendInputPack()));

    inState = 0;
    outState = 0;
    outCount = 0;
    lastOutWritten = 0;
    ui->lECusSpeed->setText("15");

    loadSettings();
    loadInFile();
}

FileHandler::~FileHandler()
{
    delete ui;
}

void FileHandler::loadSettings()
{
    presets->beginGroup("files");
    ui->lECusSpeed->setText(presets->value("cusSpeed", "15").toString());

    QString p;
    p = presets->value("inPath", QDir::home().path()).toString();
    ui->lInPath->setText(ui->lInPath->fontMetrics().elidedText(p, Qt::ElideMiddle, 210));
    ui->lInPath->setToolTip(p);

    p = presets->value("outPath", QDir::home().path()).toString();
    ui->lOutPath->setText(ui->lOutPath->fontMetrics().elidedText(p, Qt::ElideMiddle, 210));
    ui->lOutPath->setToolTip(p);
    presets->endGroup();
}

void FileHandler::saveSettings()
{
    presets->beginGroup("files");
    presets->setValue("cusSpeed", ui->lECusSpeed->text());
    presets->setValue("inPath", ui->lInPath->toolTip());
    presets->setValue("outPath", ui->lOutPath->toolTip());
    presets->endGroup();
}

void FileHandler::setSamplesWritten(int smpls)
{
    outCount = smpls - lastOutWritten;

    if(outState == 1) ui->lOutStatus->setText("Writing ... " + QString::number(outCount) + ".");
}


//-----------------------------INPUT FUNCTIONS----------------------------
void FileHandler::loadInFile()
{
    if(fin->isOpen()) fin->close();

    if(ui->lInPath->toolTip()=="")
    {
        ui->lNumLoaded->setText("No File selected!");
        finCont.clear();
        return;
    }

    fin->setFileName(ui->lInPath->toolTip());
    if(fin->open(QIODevice::ReadOnly))
    {
        QTextStream inStream(fin);
        finCont = inStream.readAll().split('\n', QString::SkipEmptyParts);

        inPos=0;
        ui->lNumLoaded->setText("/ "+QString::number(finCont.length()));
    }
    else
    {
        ui->lNumLoaded->setText(fin->errorString());
        finCont.clear();
    }
}

void FileHandler::sendInputPack()
{
    if(inState>0)
    {
        if(inPos<finCont.length())
        {
            int state;
            emit sendToFormater(finCont[inPos], 15./1000, 15./1000*inPos ,1, state);

            inPos++;
            ui->lEInPos->setText(QString::number(inPos));
            ui->hSPos->setValue((inPos*100)/finCont.length());

            if(ui->lECusSpeed->text().toInt()>10) inTimer->start(ui->lECusSpeed->text().toInt());
            else inTimer->start(10);
        }
        else on_pBInStop_clicked();
    }
}

//---------------------------UI SIGNALS------------------------------
//-------------------------------------------------------------------
void FileHandler::on_pBFormat_clicked()
{
    emit showFormater();
}

//-----------------------------OUTPUT FILE--------------------------------
void FileHandler::on_pBOutPath_clicked()
{
    QString p;
    p = QFileDialog::getSaveFileName(this, tr("Create a new file"), QDir::homePath());
    if(p.right(4)!=".txt") p += ".txt";
    ui->lOutPath->setText(ui->lInPath->fontMetrics().elidedText(p, Qt::ElideMiddle, 210));
    ui->lOutPath->setToolTip(p);
}

void FileHandler::on_pBOutPlay_clicked()
{
    if(outState>0)
    {
        outState = -1;
        ui->pBOutPlay->setText("Write");
        emit setSending(false);
        emit sigFileStop(0);
        ui->lOutStatus->setText("Paused ... " + QString::number(outCount) + ".");
    }
    else
    {
        if(outState == -1)emit sigFileStart("");
        else emit sigFileStart(ui->lOutPath->toolTip());
        outState = 1;
        ui->pBOutPlay->setText("Pause");
        emit setSending(true);
    }
}

void FileHandler::on_pBOutStop_clicked()
{
    outState = 0;
    ui->pBOutPlay->setText("Write");
    emit setSending(false);
    emit sigFileStop(1);
    ui->lOutStatus->setText("Written " + QString::number(outCount) + " samples.");
    lastOutWritten = outCount;
}

//-----------------------------INPUT FILE--------------------------------
void FileHandler::on_pBInPath_clicked()
{
    QString p;
    p = QFileDialog::getOpenFileName(this, tr("Select file"), QDir::homePath());
    ui->lInPath->setText(ui->lInPath->fontMetrics().elidedText(p, Qt::ElideMiddle, 210));
    ui->lInPath->setToolTip(p);

    loadInFile();
}

void FileHandler::on_pBInPlay_clicked()
{
    if(inState>0)
    {
        inState = -1;
        ui->pBInPlay->setText("Play");
    }
    else
    {
        inState = 1;
        ui->pBInPlay->setText("Pause");
        sendInputPack();
    }
}

void FileHandler::on_pBInStop_clicked()
{
    inState = 0;
    inPos = 0;
    ui->pBInPlay->setText("Play");
    ui->hSPos->setValue(0);
    ui->lEInPos->setText("0");
}

void FileHandler::on_hSPos_sliderMoved(int position)
{
    inPos = (finCont.length()*position)/100;
}


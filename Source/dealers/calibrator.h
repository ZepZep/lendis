#ifndef CALIBRATOR_H
#define CALIBRATOR_H

#include <math.h>
#include <QObject>
#include <QVector3D>
#include <QQuaternion>
#include <QSettings>

#include "visuals/calstatus.h"
#include "utils/putils.h"
#include "utils/brain.h"


class Calibrator : public QObject
{
    Q_OBJECT

public:
    Calibrator(QWidget *parent);
    ~Calibrator();

    void setTolAcc0(float tol);
    void setTolAcc1(float tol);
    void setTolGyr0(float tol);
    void setTolGyr1(float tol);

    Brain *brain;

private:
    int getMovementMod(QVector3D &acc, QVector3D gyr, QVector3D &nnS, int what);
    void loadSettings();

    CalStatus *winCalStatus;
    QVector3D magMin, magMax;
    float MagLocal;
    float tolAcc1, tolGyr1, tolAcc2, tolGyr2;
    int stillNeed;

    QSettings *presets;
    Kalman *kalMag;

    bool calibration;
    int samples;
    QVector3D gyrCal;
    QVector3D gyrCalTemp;
    QVector3D accCal;
    QVector3D accCalTemp;
    QVector3D magCal;
    QVector3D magCalTemp;
    int moveHist;

    QVector<QVector3D> histAcc, histMag, histGyr;
    QVector<float> histFTime;
    QVector<int> histMod;
    int histPos;

    Avrg* accAvrg;

    int detType;

signals:
    void sigCalEnd();
    void addCalData(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, int mod, QVector3D brainstatus);
    void setOrientation(QQuaternion quat);

public slots:
    void rawPackRecieve(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime);
    void calibrate();
    void calEnd();
    void reset();
    void saveSettings();

    void setNN(QString path);
};

#endif // CALIBRATOR_H

#include "derotator.h"
#include "qquaternion.h"
#include <QQuaternion>

Derotator::Derotator(QObject *parent) : QObject(parent)
{
    presets = new QSettings("config.ini", QSettings::IniFormat);
    reset();
}


Derotator::~Derotator()
{

}

void Derotator::loadSettings()
{
    presets->beginGroup("settings");
    gyroOnly = presets->value("gyroOnly", false).toBool();
    presets->endGroup();
}

bool Derotator::isStalling()
{
    QVector3D v(1, 0, 0);
    v = orientation.rotatedVector(v);

    if(v.z()>0.9) return true;
    if(v.z()<-0.9) return true;

    return false;
}

void Derotator::calPackRecieve(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, int mod, QVector3D BrainStatus)
{
    if(gyroOnly) mod = -1;
    //if(mod != 1) mod = -1;
    //mod = 0;
    if(isStalling())
    {
        mod = -1;
        emit setMovementMode(-1);
    }
    else emit setMovementMode(mod);

    if(mod == 1) acc.normalize();

    //qDebug() <<mod;
    if(mod == 0 or mod == -1) //----------------------Movement or Stalling------------------------------------
    {
        QQuaternion deltaFrameS, deltaFrameL;
        num = 0;
        avga.setX(0);avga.setY(0);avga.setZ(0);

        float angle = gyr.length() * frameTime;
        deltaFrameS = QQuaternion::fromAxisAndAngle(gyr, angle);
        //deltaFrameL = QQuaternion::fromAxisAndAngle(gyr.x(), gyr.z(), gyr.y(), angle);
        gyr = gyr/180*M_PI;
        float omegaMagnitude = gyr.length();
        gyr.setX(gyr.x()/omegaMagnitude);
        gyr.setY(gyr.y()/omegaMagnitude);
        gyr.setZ(gyr.z()/omegaMagnitude);
        float theta = omegaMagnitude * frameTime;
        float sintheta = sin(theta/2);
        float costheta = cos(theta/2);
        QQuaternion delta;
        delta.setX(sintheta*gyr.x());
        delta.setY(sintheta*gyr.y());
        delta.setZ(sintheta*gyr.z());
        delta.setScalar(costheta);
        deltaFrameL = delta.normalized();

        orientation =  orientation*deltaFrameS;

        //qDebug() <<deltaFrameL - deltaFrameS;
        //orientation = orientation * deltaFrameS;

        if(mod != -1)
        {
            //remove heading
            headingGyr = headingFromQuat(orientation);
            orientation = QQuaternion::fromAxisAndAngle(0, 0, -1, headingGyr*180/M_PI) * orientation;
        }
    }
    else //-------------------------------Still--------------------------------------
    {
        float alfa, cosAlfa;
        float rotVecX, rotVecY, rotVecZ;
        float accXlast=0, accYlast=0, accZlast=1;

        cosAlfa=accXlast*acc.x()+accYlast*acc.y()+accZlast*acc.z();
        alfa=acos(cosAlfa);

        rotVecX=1/sin(alfa)*((accYlast*acc.z()-accZlast*acc.y()));
        rotVecY=1/sin(alfa)*((accZlast*acc.x()-accXlast*acc.z()));
        rotVecZ=1/sin(alfa)*((accXlast*acc.y()-accYlast*acc.x()));

        orientation = QQuaternion::fromAxisAndAngle(rotVecX, rotVecY, rotVecZ, -alfa*180/M_PI);

        //remove parasite heading
        float zrot = headingFromQuat(orientation);
        orientation = QQuaternion::fromAxisAndAngle(0, 0, -1, zrot*180/M_PI) * orientation; //zrot*180/M_PI rotToFlat <=> orientation
    }

    //-------------------------------HEADING FROM MAGNETOMETER------------------------------
    QQuaternion rotToFlat = orientation;
    QVector3D magFlat = rotToFlat.rotatedVector(mag);

    if(magFlat.y()>0) headingMag=M_PI/2-atan(magFlat.x()/magFlat.y());
    else if(magFlat.y()<0) headingMag=3*M_PI/2-atan(magFlat.x()/magFlat.y());
    else if(magFlat.y()==0)
    {
        if(magFlat.x()<0) headingMag=M_PI;
        else headingMag=0;
    }

    //add heading (gyr 1/mag -1)
    if(mod != -1) orientation = QQuaternion::fromAxisAndAngle(0, 0, -1, headingMag*180/M_PI) * orientation;

    //------------------------------Derotation--------------------------------------------
    QVector3D accDerot = orientation.rotatedVector(acc);
    //   if(mod == 1)
    //   {
    //       num+=1;
    //        avga.setX(((num-1)*avga.x()+accDerot.x())/num);
    //        avga.setY(((num-1)*avga.y()+accDerot.y())/num);
    //        avga.setZ(0);
    //        accDerot=accDerot-avga;
    //   }
    emit addDerotData(accDerot, orientation, frameTime, wholeTime, mod, BrainStatus);
}

void Derotator::reset()
{
    orientation = QQuaternion(1, 0, 0, 0);
    phi = 0;
    theta = 0;
    headingGyr = 0;
    headingMag = 0;
    num = 0;
    avga.setX(0);avga.setY(0);avga.setZ(0);
    loadSettings();
}

void Derotator::setOrientation(QQuaternion quat)
{
    orientation = quat;
}

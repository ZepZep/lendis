#ifndef DEROTATOR_H
#define DEROTATOR_H

#include <math.h>
#include <QObject>
#include <QVector3D>
#include <QQuaternion>
#include <QSettings>
#include "utils/putils.h"

class Derotator : public QObject
{
    Q_OBJECT
public:
    explicit Derotator(QObject *parent = 0);
    ~Derotator();

private:
    void loadSettings();
    bool isStalling();

    QQuaternion orientation;
    QSettings *presets;
    QVector3D avga;
    float phi, theta, headingGyr, headingMag, num;
    bool gyroOnly;

signals:
    void addDerotData(QVector3D acc,  QQuaternion orientation, float frameTime, float wholeTime, int mod, QVector3D BrainStatus);
    void setMovementMode(int mode);

public slots:
    void calPackRecieve(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, int mod, QVector3D BrainStatus);
    void reset();
    void setOrientation(QQuaternion quat);
};

#endif // DEROTATOR_H

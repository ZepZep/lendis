#ifndef RECIEVER_H
#define RECIEVER_H

#include <QObject>
#include <QSerialPort>
#include <QVector3D>
#include <QList>
#include <QTimer>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QSettings>


class Reciever : public QObject
{
    Q_OBJECT

public:
    explicit Reciever(QWidget *parent = 0);
    ~Reciever();

private:
    void closeEvent(QCloseEvent *event);
    void loadSettings();
    void saveSettings();
//    void loadInFile();
    void prepPack(QString pack);

    QSerialPort *serial;
    QVector3D acc, gyr, mag;
    QVector3D accMin, accMax, magMin, magMax;
    QString actualReading;
    QSettings *presets;

    float magLSB_Gaus;
    float gyrMDPS_LSB;
    float accMg_LSB;

    int lastTime;
    int samples;

    bool useButtonReset, buttonReset;

private slots:
    void serialRecived();

public slots:
    void connectSerial(QString address);
    void disconnectSer();
    void restart();

signals:
    void rawPackSend(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime, bool button);
    void calibrate();
    void reset();

    void sendToFormater(QString in, float frameTime, float wholeTime, int where, int &state);
};

#endif // RECIEVER_H

#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <QWidget>
#include <QVector3D>
#include <QStringList>
#include <QTimer>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QSettings>

namespace Ui {
class FileHandler;
}

class FileHandler : public QWidget
{
    Q_OBJECT

public:
    explicit FileHandler(QWidget *parent = 0);
    ~FileHandler();

    void setSamplesWritten(int smpls);

private:
    Ui::FileHandler *ui;

    void loadSettings();
    void loadInFile();

    QSettings *presets;

    QFile *fin;
    int inPos, outCount;
    int lastOutWritten;
    int inState, outState;

    QTimer *inTimer;

    QStringList finCont;

private slots:
    void sendInputPack();

    void on_pBFormat_clicked();

    void on_pBOutPath_clicked();
    void on_pBOutPlay_clicked();
    void on_pBOutStop_clicked();

    void on_pBInPath_clicked();
    void on_pBInPlay_clicked();
    void on_pBInStop_clicked();
    void on_hSPos_sliderMoved(int position);

public slots:
    void saveSettings();

signals:
    void showFormater();

    void sigFileStart(QString path);
    void sigFileStop(bool hard);

    void setSending(bool value);
    void sendToFormater(QString in, float frameTime, float wholeTime, int where, int &state);
};

#endif // FILEHANDLER_H

#include "reciever.h"

#include <QDebug>

Reciever::Reciever(QWidget *parent) : QObject(parent)
{
    //Sensor Units Conversion Constants
    magLSB_Gaus = 1/6.842e3; //Gauss
    gyrMDPS_LSB = 17.5e-3;
    accMg_LSB = 0.061e-3;

    //Hardware deformation constants (scale, offset)
    accMin = QVector3D(-1.0055485, -0.9946478, -0.9949894);
    accMax = QVector3D( 1.0008940,  1.0017908,  1.0239429);

    presets = new QSettings("config.ini", QSettings::IniFormat);

    serial = new QSerialPort(this);

    buttonReset=false;
    samples = 0;

    QString labelFormatTooltip = "Mag 1 0 2\nGyr 3 4 5\nAcc 6 7 8\nTime 9\nButton 10";
}

Reciever::~Reciever()
{
    serial->close();
}

void Reciever::closeEvent(QCloseEvent *event)
{
    saveSettings();
    //event->accept();
}

void Reciever::loadSettings()
{
    presets->beginGroup("settings");
    useButtonReset = presets->value("buttonReset", false).toBool();
    presets->endGroup();
}

void Reciever::saveSettings()
{

}

void Reciever::restart()
{
    loadSettings();
}

//--------------------------------------SERIAL PORT----------------------------------------------
void Reciever::connectSerial(QString address)
{
    if(serial->isOpen())serial->close();

    serial->setPortName(address);
    serial->setBaudRate(QSerialPort::Baud115200);
    serial->setDataBits(QSerialPort::Data8);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    if(serial->open(QIODevice::ReadWrite))
    {
        connect(serial, SIGNAL(readyRead()), this, SLOT(serialRecived()));
        samples = 0;
    }
    else
    {
        qDebug() <<serial->errorString();
    }
}

void Reciever::disconnectSer()
{
    if(serial->isOpen())serial->close();
}

//---------------------------------DEAL WITH INCOMMING DATA----------------------------------------
void Reciever::serialRecived()
{
    QByteArray data = serial->readAll();
    actualReading+=QString(data);

    if(actualReading.contains('\n'))
    {
        QStringList slist=actualReading.split('\n');

        for(int i = 0; i<slist.length()-1; i++)
        {
            prepPack(slist[i]);
        }
        actualReading=slist.last();
    }
}

void Reciever::prepPack(QString pack)
{
    samples++;
    QStringList list=pack.split(" ");
    int rawData[12];

    for(int i=0; i<list.length() and i<=10; i++)
    {
        rawData[i]=list.at(i).toInt(0, 10);
    }

    int elapTime=rawData[9];
    int frameTime=elapTime-lastTime;
    lastTime=elapTime;

    //Conversion Constants
    acc.setX(rawData[6]*accMg_LSB);
    acc.setY(rawData[7]*accMg_LSB);
    acc.setZ(rawData[8]*accMg_LSB);

    gyr.setX(rawData[3]*gyrMDPS_LSB);
    gyr.setY(rawData[4]*gyrMDPS_LSB);
    gyr.setZ(rawData[5]*gyrMDPS_LSB);

    mag.setX(rawData[1]*magLSB_Gaus);
    mag.setY(-rawData[0]*magLSB_Gaus);
    mag.setZ(rawData[2]*magLSB_Gaus);

    //Offset
    acc.setX(acc.x()-((accMax.x()+accMin.x())/2));
    acc.setY(acc.y()-((accMax.y()+accMin.y())/2));
    acc.setZ(acc.z()-((accMax.z()+accMin.z())/2));

    //Scale
    acc.setX(acc.x()/((accMax.x()-accMin.x())/2));
    acc.setY(acc.y()/((accMax.y()-accMin.y())/2));
    acc.setZ(acc.z()/((accMax.z()-accMin.z())/2));

    bool button = false;
    if(rawData[10]) button = true;

    if(useButtonReset) //Restart with arduino button
    {
        if(rawData[10])
        {
            if(!buttonReset)
            {
                emit reset();
                buttonReset = true;
            }
        }
        else buttonReset = false;
    }

    //Send pack
    if(samples >= 20)
    {
        if(samples == 20) emit calibrate();
        emit rawPackSend(acc, gyr, mag, frameTime*1./1000, elapTime*1./1000, button);
    }
}

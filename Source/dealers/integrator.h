#ifndef INTEGRATOR_H
#define INTEGRATOR_H

#include <QObject>
#include <math.h>
#include <QVector3D>
#include <QQuaternion>
#include <QSettings>

#include "utils/putils.h"

class Integrator : public QObject
{
    Q_OBJECT
public:
    explicit Integrator(QObject *parent = 0);
    ~Integrator();

private:
    void loadSettings();

    QVector3D vel, pos, avga, avgv;
    int num;
    Kalman *kalAcc;

//    Avrg *avgAcc;

//    int counter;

//    QVector3D kAvg;
//    QVector3D p, q, r, k;

    QSettings *presets;

signals:
    void addFinalData(QVector3D velocity, QVector3D position, QVector3D acc, QVector3D kalAcc, QQuaternion orientation, float frameTime, float wholeTime);

public slots:
    void derotDataRecieve(QVector3D acc, QQuaternion orientation, float frameTime, float wholeTime, int mod, QVector3D BrainStatus);
    void reset();
};

#endif // INTEGRATOR_H

#include "calibrator.h"
#include <QDebug>

Calibrator::Calibrator(QWidget *parent = 0):
    QObject(parent)
{
    presets = new QSettings("config.ini", QSettings::IniFormat);
    brain = new Brain;
    loadSettings();
    calibration = false;
    winCalStatus = new CalStatus(2000);
    connect(winCalStatus, SIGNAL(calEnd()), this, SLOT(calEnd()));

    moveHist = 0;

    magMin = QVector3D(-0.650, -0.640, 0.220);
    magMax = QVector3D( 0.325,  0.320, 1.150);
    MagLocal = 489.117; //Tomanova [mGauss]
    kalMag = new Kalman(1,0.5,5,10);
    accAvrg = new Avrg(0);
}

Calibrator::~Calibrator()
{

}

void Calibrator::loadSettings()
{
    presets->beginGroup("calibrator");
    stillNeed = presets->value("stillNeed", 3).toInt();
    tolAcc1 = presets->value("tolAcc1", 0.01).toDouble();
    tolGyr1 = presets->value("tolGyr1",  3).toDouble();
    presets->endGroup();

    histAcc.resize(stillNeed);
    histGyr.resize(stillNeed);
    histMag.resize(stillNeed);
    histFTime.resize(stillNeed);
    histMod.resize(stillNeed);
    histPos = 0;

    presets->beginGroup("neural");
    detType = 0;
    if(presets->value("useSimple", true).toBool()) detType--;
    if(presets->value("useNN", true).toBool()) detType++;
    presets->endGroup();

    brain->loadSettings();
}

void Calibrator::saveSettings()
{
    winCalStatus->close();
}

void Calibrator::setNN(QString path)
{
    brain->setNetwork(path);
}


void Calibrator::rawPackRecieve(QVector3D acc, QVector3D gyr, QVector3D mag, float frameTime, float wholeTime)
{
    int mod;

    //Mag offset
    mag.setX(mag.x()-((magMax.x()+magMin.x())/2));
    mag.setY(mag.y()-((magMax.y()+magMin.y())/2));
    mag.setZ(mag.z()-((magMax.z()+magMin.z())/2));

    //Mag scale
    mag.setX(mag.x()/((magMax.x()-magMin.x())/2));
    mag.setY(mag.y()/((magMax.y()-magMin.y())/2));
    mag.setZ(mag.z()/((magMax.z()-magMin.z())/2));

    //Magnetic local field strength
    mag = MagLocal * mag;

    //KF for mag
    mag = kalMag->getrValue(mag);

    if(calibration)
    {
        samples++;
        accCalTemp+=acc;
        gyrCalTemp+=gyr;
        magCalTemp+=mag;
    }
    else
    {
        QVector3D nnVector;
        mod = getMovementMod(acc, gyr-gyrCal, nnVector, detType); //-1 silly, 0 both, 1 brain

        //-----------SHIFT-----------
        int aPos = histPos%stillNeed;
        histAcc[aPos] = acc;
        histGyr[aPos] = gyr;
        histMag[aPos] = mag;
        histFTime[aPos] = frameTime;
        histMod[aPos] = mod;

        aPos = (aPos+1)%stillNeed;
        histPos++;

        acc = histAcc[aPos];
        gyr =  histGyr[aPos];
        mag = histMag[aPos];
        frameTime = histFTime[aPos];
        if(histMod[aPos]==0) mod = 0;

        //correct gyrCal
        if(mod == 1)
        {
          //gyrCal = (99*gyrCal+gyr)/100;
        }
        gyr -= gyrCal;

        if(histPos > stillNeed)
            emit addCalData(acc, gyr, mag, frameTime, wholeTime, mod, nnVector);
    }
}

void Calibrator::calEnd()
{
    accCal.setX(accCalTemp.x()/samples);
    accCal.setY(accCalTemp.y()/samples);
    accCal.setZ(accCalTemp.z()/samples);
    gyrCal.setX(gyrCalTemp.x()/samples);
    gyrCal.setY(gyrCalTemp.y()/samples);
    gyrCal.setZ(gyrCalTemp.z()/samples);
    magCal.setX(magCalTemp.x()/samples);
    magCal.setY(magCalTemp.y()/samples);
    magCal.setZ(magCalTemp.z()/samples);

    accCal.normalize();

    float alfa, cosAlfa;
    float rotVecX, rotVecY, rotVecZ;
    float accXlast=0, accYlast=0, accZlast=1;

    cosAlfa=accXlast*accCal.x()+accYlast*accCal.y()+accZlast*accCal.z();
    alfa=acos(cosAlfa);

    rotVecX=1/sin(alfa)*((accYlast*accCal.z()-accZlast*accCal.y()));
    rotVecY=1/sin(alfa)*((accZlast*accCal.x()-accXlast*accCal.z()));
    rotVecZ=1/sin(alfa)*((accXlast*accCal.y()-accYlast*accCal.x()));

    QQuaternion orientation = QQuaternion::fromAxisAndAngle(rotVecX, rotVecY, rotVecZ, -alfa*180/M_PI);

    float parasite = headingFromQuat(orientation);
    orientation = QQuaternion::fromAxisAndAngle(0, 0, -1, parasite*180/M_PI) * orientation;
    QVector3D magFlat = orientation.rotatedVector(magCal);
    float headingMag = 10;

    if(magFlat.y()>0) headingMag=M_PI/2-atan(magFlat.x()/magFlat.y());
    else if(magFlat.y()<0) headingMag=3*M_PI/2-atan(magFlat.x()/magFlat.y());
    else if(magFlat.y()==0)
    {
        if(magFlat.x()<0) headingMag=M_PI;
        else headingMag=0;
    }

    qDebug() <<"Orientation:" <<headingMag*180/M_PI;

    orientation = QQuaternion::fromAxisAndAngle(0, 0, -1, headingMag*180/M_PI) * orientation;

    calibration=false;
    emit sigCalEnd();
    emit setOrientation(orientation);
}

void Calibrator::reset()
{
    loadSettings();
    accAvrg->reset();
}

void Calibrator::calibrate()
{
    samples=0;
    accCalTemp=QVector3D(0, 0, 0);
    gyrCalTemp=QVector3D(0, 0, 0);
    magCalTemp=QVector3D(0, 0, 0);
    calibration=true;
    winCalStatus->start();
}

int Calibrator::getMovementMod(QVector3D &acc, QVector3D gyr, QVector3D &nnS, int what)
{
    int sillyMode = 0;
    if(abs(acc.length()-1)<tolAcc1 and abs(gyr.x())<tolGyr1 and abs(gyr.y())<tolGyr1 and abs(gyr.z())<tolGyr1)
    {
        moveHist++;
        if(moveHist >= stillNeed)
        {
            sillyMode = 1;
        }
    }
    else
    {
        moveHist = 0;
        sillyMode = 0;
    }

    float brainValue, brainKalValue;
    int brainMode = brain->decide(acc, gyr, brainValue, brainKalValue);

    int retVal;

    if(brainMode == 0)
    {
        accAvrg->reset();
    }
    else if(sillyMode == 1)
    {
        //acc = accAvrg->getrAvr(acc);
    }

    if(what == 0)
    {
        if(brainMode==1 and sillyMode==1) retVal = 1;
        else retVal = 0;
    }
    else if(what == -1) retVal = sillyMode;
    else if(what == 1) retVal = brainMode;
    //else retVal = 0;

    nnS = QVector3D(brainKalValue, brainValue, retVal);
    return retVal;
}

#include "dealers/reciever.h"
#include "dealers/calibrator.h"
#include "dealers/derotator.h"
#include "dealers/integrator.h"
#include "ui/vizualizer.h"
#include "ui/cubedialog.h"
#include "ui/settings.h"
#include "ui/packformater.h"

#include <QApplication>
#include <QProcess>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Vizualizer *vizualizer;
    vizualizer = new Vizualizer();
    Settings *settings = new Settings();
    Derotator *derotator = new Derotator(vizualizer);
    Integrator *integrator = new Integrator(vizualizer);
    Calibrator *calibrator = new Calibrator(vizualizer);
    Reciever *reciever = new Reciever();
    CubeDialog *cubeDialog = new CubeDialog();
    packFormater *formater = new packFormater();

    //    QProcess *proc = new QProcess;
    //    proc->start("kde-config --path icon");

    QObject::connect(vizualizer, SIGNAL(sigConnect(QString)), reciever, SLOT(connectSerial(QString)));
    QObject::connect(vizualizer, SIGNAL(sigDisconnect()), reciever, SLOT(disconnectSer()));
    QObject::connect(vizualizer, SIGNAL(sigCalibrate()), calibrator, SLOT(calibrate()));
    QObject::connect(vizualizer, SIGNAL(sigClosing()), cubeDialog, SLOT(close()));
    QObject::connect(vizualizer, SIGNAL(sigClosing()), formater, SLOT(close()));
    QObject::connect(vizualizer, SIGNAL(sigClosing()), vizualizer->fDialog, SLOT(close()));
    QObject::connect(vizualizer, SIGNAL(sigClosing()), settings, SLOT(close()));
    QObject::connect(vizualizer, SIGNAL(sigClosing()), vizualizer->fileHand, SLOT(saveSettings()));
    QObject::connect(vizualizer, SIGNAL(sigClosing()), calibrator, SLOT(saveSettings()));
    QObject::connect(vizualizer, SIGNAL(sigWriteFormated(QVector<QVector3D>,QVector<double>)), formater, SLOT(write(QVector<QVector3D>,QVector<double>)));
    QObject::connect(vizualizer, SIGNAL(sigUpdateAll()), cubeDialog, SLOT(updateAll()));
    QObject::connect(vizualizer, SIGNAL(sigCubeShow()), cubeDialog, SLOT(show()));
    QObject::connect(vizualizer, SIGNAL(sigSettingsShow()), settings, SLOT(show()));
    QObject::connect(vizualizer->fileHand, SIGNAL(showFormater()), formater, SLOT(show()));
    QObject::connect(settings, SIGNAL(nnChanged(QString)), calibrator, SLOT(setNN(QString)));

    QObject::connect(settings, SIGNAL(save()), vizualizer, SLOT(restart()));
    QObject::connect(reciever, SIGNAL(reset()), vizualizer, SLOT(restart()));
    QObject::connect(cubeDialog, SIGNAL(sigReset()),vizualizer , SLOT(restart()));
    QObject::connect(calibrator, SIGNAL(sigCalEnd()),vizualizer , SLOT(restart()));
    QObject::connect(vizualizer, SIGNAL(sigReset()),calibrator , SLOT(reset()));
    QObject::connect(vizualizer, SIGNAL(sigReset()),derotator , SLOT(reset()));
    QObject::connect(vizualizer, SIGNAL(sigReset()),integrator , SLOT(reset()));
    QObject::connect(vizualizer, SIGNAL(sigReset()),cubeDialog , SLOT(reset()));
    QObject::connect(vizualizer, SIGNAL(sigReset()),reciever , SLOT(restart()));
    QObject::connect(calibrator, SIGNAL(setOrientation(QQuaternion)),derotator , SLOT(setOrientation(QQuaternion)));

    QObject::connect(reciever, SIGNAL(calibrate()), calibrator, SLOT(calibrate()));
    QObject::connect(cubeDialog, SIGNAL(sigCalibrate()), calibrator, SLOT(calibrate()));

    QObject::connect(vizualizer->fileHand, SIGNAL(setSending(bool)), vizualizer, SLOT(setSending(bool)));
    QObject::connect(vizualizer->fileHand, SIGNAL(sigFileStart(QString)), formater, SLOT(startFileWrite(QString)));
    QObject::connect(vizualizer->fileHand, SIGNAL(sigFileStop(bool)), formater, SLOT(stopFileWrite(bool)));
    QObject::connect(vizualizer->fileHand, SIGNAL(sendToFormater(QString,float,float,int,int&)), formater, SLOT(sendPackFromString(QString,float,float,int,int&)));

    QObject::connect(reciever, SIGNAL(rawPackSend(QVector3D,QVector3D,QVector3D,float,float,bool)), vizualizer, SLOT(addRawData(QVector3D,QVector3D,QVector3D,float,float,bool)));
    QObject::connect(reciever, SIGNAL(rawPackSend(QVector3D,QVector3D,QVector3D,float,float,bool)), calibrator, SLOT(rawPackRecieve(QVector3D,QVector3D,QVector3D,float,float)));
    QObject::connect(formater, SIGNAL(sendToCal(QVector3D,QVector3D,QVector3D,float,float,bool)), vizualizer, SLOT(addRawData(QVector3D,QVector3D,QVector3D,float,float,bool)));
    QObject::connect(formater, SIGNAL(sendToCal(QVector3D,QVector3D,QVector3D,float,float,bool)), calibrator, SLOT(rawPackRecieve(QVector3D,QVector3D,QVector3D,float,float)));

    QObject::connect(calibrator, SIGNAL(addCalData(QVector3D,QVector3D,QVector3D,float,float,int,QVector3D)), vizualizer, SLOT(addCalData(QVector3D,QVector3D,QVector3D,float,float,int,QVector3D)));
    QObject::connect(calibrator, SIGNAL(addCalData(QVector3D,QVector3D,QVector3D,float,float,int, QVector3D)), derotator, SLOT(calPackRecieve(QVector3D,QVector3D,QVector3D,float,float,int,QVector3D)));

    QObject::connect(derotator, SIGNAL(addDerotData(QVector3D,QQuaternion,float,float,int, QVector3D)), integrator, SLOT(derotDataRecieve(QVector3D,QQuaternion,float,float,int, QVector3D)));
    QObject::connect(derotator, SIGNAL(setMovementMode(int)), vizualizer->compass, SLOT(setMovementMod(int)));

    QObject::connect(integrator, SIGNAL(addFinalData(QVector3D,QVector3D,QVector3D,QVector3D,QQuaternion,float,float)), vizualizer, SLOT(addIntegData(QVector3D,QVector3D,QVector3D,QVector3D,QQuaternion,float,float)));
    QObject::connect(integrator, SIGNAL(addFinalData(QVector3D,QVector3D,QVector3D,QVector3D,QQuaternion,float,float)), cubeDialog, SLOT(addDerotData(QVector3D,QVector3D,QVector3D,QVector3D,QQuaternion,float,float)));

    //calibrator->setNN("D:/Lenka/Dis/git/build/debug/NNs/TDNN_1.txt");
    settings->autochooseNN();
    vizualizer->show();

    vizualizer->reconnect();
    //reciever->connectSerial("ttyACM0");
    //reciever->connectSerial("com3");

    return a.exec();
}

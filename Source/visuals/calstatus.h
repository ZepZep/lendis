#ifndef CALSTATUS_H
#define CALSTATUS_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class CalStatus;
}

class CalStatus : public QDialog
{
    Q_OBJECT

public:
    explicit CalStatus(int lenght, QWidget *parent = 0);
    ~CalStatus();

    void start();
    int maximum;

protected:
    int status;
    QTimer *timerC;

private:
    Ui::CalStatus *ui;

signals:
    void calEnd();

private slots:
    void timerEnded();
};

#endif // CALSTATUS_H

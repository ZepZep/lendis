#include "glcubewidget.h"

glCubeWidget::glCubeWidget(QWidget *parent) : QGLWidget(parent)
{
    animate = 0;
    targetFPS = 25;
    animator = 0;

    resetView();
}

glCubeWidget::~glCubeWidget()
{

}

void glCubeWidget::callUpdate()
{
    if(animate)
    {
        animator+=0.1;
        updateVectors(QQuaternion(sin(animator), cos(animator), 1, 0).normalized(), QVector3D(0, 0, 0));
    }
    if(this->isVisible()) update();
}

void glCubeWidget::updateVectors(QQuaternion orientation, QVector3D pos)
{
    pos= pos*QVector3D(10, 10, 10);

    position.setX(pos.x());
    position.setY(pos.y());
    position.setZ(pos.z());

    lastOrient = orientation;

    QVector3D vects[8];
    vects[0]=QVector3D(-1.5, -0.7,  1);
    vects[1]=QVector3D( 1.5, -0.7,  1);
    vects[2]=QVector3D( 1.5, -0.7, -1);
    vects[3]=QVector3D(-1.5, -0.7, -1);
    vects[4]=QVector3D(-1.5,  0.7,  1);
    vects[5]=QVector3D( 1.5,  0.7,  1);
    vects[6]=QVector3D( 1.5,  0.7, -1);
    vects[7]=QVector3D(-1.5,  0.7, -1);

    orientation = afterRot*orientation;

    for(int i=0; i<8; i++)
    {
        // x = -z
        // y = x
        // z = y
        float aaa = vects[i].z();
        vects[i].setZ(vects[i].y());
        vects[i].setY(vects[i].x());
        vects[i].setX(aaa);

        vects[i]=orientation.rotatedVector(vects[i])+position;

        aaa = vects[i].y();
        vects[i].setY(vects[i].z());
        vects[i].setZ(vects[i].x());
        vects[i].setX(aaa);

//        aaa = vects[i].z();
//        vects[i].setZ(vects[i].x());
//        vects[i].setX(-aaa);
    }

    A=vects[0];
    B=vects[1];
    C=vects[2];
    D=vects[3];
    E=vects[4];
    F=vects[5];
    G=vects[6];
    H=vects[7];
}

void glCubeWidget::setFPS(int fps)
{
    targetFPS = fps;
}

void glCubeWidget::autoView()
{
    afterRot=lastOrient;
    afterRot.setScalar(-afterRot.scalar());
}

void glCubeWidget::resetView()
{
    distance = 20;
    afterRot = QQuaternion(1, 0, 0, 0).normalized();
    position = QVector3D(0, 0, 0);
}

void glCubeWidget::setViewDistance(float dist)
{
    viewDist=dist;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum( -leftRight, leftRight, -topBottom, topBottom, 5.0, viewDist);
    glMatrixMode( GL_MODELVIEW );
}

void glCubeWidget::mouseMoveEvent(QMouseEvent *event)
{
    float dx = event->x() - lastPos.x();
    float dy = event->y() - lastPos.y();

    if(dx<10 and dx>-10 and dy<10 and dy>-10)
    {
        dx = dx/this->width()*180;
        dy = dy/this->height()*180;

        if (event->buttons() & Qt::LeftButton)
        {
            QVector3D xVect(0, 0, 1);
            QVector3D yVect(1, 0, 0);
            QQuaternion temp = afterRot;
            temp.setScalar(-temp.scalar());
            afterRot = afterRot * QQuaternion::fromAxisAndAngle(temp.rotatedVector(xVect), dx);
            afterRot = afterRot * QQuaternion::fromAxisAndAngle(temp.rotatedVector(yVect), dy);
        }
    }
    lastPos = event->pos();
}

void glCubeWidget::wheelEvent(QWheelEvent *event)
{
    if(event->delta()>0)
    {
        distance-=1;
    }
    else
    {
        distance+=1;
    }

    if(distance < 10) distance=10;
}

void glCubeWidget::initializeGL()
{
    //activate the depth buffer
    glEnable(GL_DEPTH_TEST);
    timer=new QTimer;
    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(callUpdate()));
}

void glCubeWidget::resizeGL (int width, int height)
{
    leftRight = width;
    topBottom = height;

    float divid = std::min(width, height);

    leftRight /= divid;
    topBottom /= divid;

    glViewport( 0, 0, (GLint)width, (GLint)height );

    /* create viewing cone with near and far clipping planes */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum( -leftRight, leftRight, -topBottom, topBottom, 5.0, viewDist);

    glMatrixMode( GL_MODELVIEW );
}

void glCubeWidget::paintGL(){

    //delete color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -distance); //move along z-axis

    /* create 3D-Cube */

    glBegin(GL_QUADS);

    //front
    glColor3f(0,0.0,1.0);

    glVertex3f(A.x(), A.y(), A.z()); //A
    glVertex3f(B.x(), B.y(), B.z()); //B
    glVertex3f(F.x(), F.y(), F.z()); //F
    glVertex3f(E.x(), E.y(), E.z()); //E

    //back

    glColor3f(1.0,1.0,0.0);

    glVertex3f(D.x(), D.y(), D.z()); //D
    glVertex3f(C.x(), C.y(), C.z()); //C
    glVertex3f(G.x(), G.y(), G.z()); //G
    glVertex3f(H.x(), H.y(), H.z()); //H

    //top
    glColor3f(1.0,0.0,0.0);

    glVertex3f(E.x(), E.y(), E.z()); //E
    glVertex3f(F.x(), F.y(), F.z()); //F
    glVertex3f(G.x(), G.y(), G.z()); //G
    glVertex3f(H.x(), H.y(), H.z()); //H

    //bottom
    glColor3f(0.0,1.0,1.0);

    glVertex3f(A.x(), A.y(), A.z()); //A
    glVertex3f(B.x(), B.y(), B.z()); //B
    glVertex3f(C.x(), C.y(), C.z()); //C
    glVertex3f(D.x(), D.y(), D.z()); //D

    //right
    glColor3f(0.0,1.0,0.0);

    glVertex3f(B.x(), B.y(), B.z()); //B
    glVertex3f(C.x(), C.y(), C.z()); //C
    glVertex3f(G.x(), G.y(), G.z()); //G
    glVertex3f(F.x(), F.y(), F.z()); //F

    //left
    glColor3f(1.0,0.0,1.0);

    glVertex3f(A.x(), A.y(), A.z()); //A
    glVertex3f(D.x(), D.y(), D.z()); //D
    glVertex3f(H.x(), H.y(), H.z()); //H
    glVertex3f(E.x(), E.y(), E.z()); //E

    glEnd();
}

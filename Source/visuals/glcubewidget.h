#ifndef GLCUBEWIDGET_H
#define GLCUBEWIDGET_H

#include <QtOpenGL>
#include <QGLWidget>
#include <QTimer>
#include <QQuaternion>

class glCubeWidget : public QGLWidget
{
    Q_OBJECT
public:
    glCubeWidget(QWidget *parent = 0);
    ~glCubeWidget();

    GLvoid ReSizeGLScene(GLsizei width, GLsizei height);

    void updateVectors(QQuaternion orientation, QVector3D pos);
    void setFPS(int fps);

    void autoView();
    void resetView();
    void setViewDistance(float dist);

protected:
    // Set up the rendering context, define display lists etc.:
   void initializeGL();
   // draw the scene:
   void paintGL();
   // setup viewport, projection etc.:
   void resizeGL (int width, int height);

   void mouseMoveEvent(QMouseEvent *event);
   void wheelEvent(QWheelEvent *event);

   int targetFPS;
   QQuaternion afterRot;
   QQuaternion lastOrient;
   QPoint lastPos;
   QVector3D position;
   bool animate;
   float animator;
   float viewDist;
   float distance;
   float leftRight, topBottom;
   QTimer* timer;
   QVector3D A, B, C, D, E, F, G, H;

public slots:
   void callUpdate();
};

#endif // GLCUBEWIDGET_H

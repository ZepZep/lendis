#include "calstatus.h"
#include "ui_calstatus.h"

CalStatus::CalStatus(int lenght, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalStatus)
{
    ui->setupUi(this);

    maximum=lenght;

    timerC = new QTimer;
    connect(timerC, SIGNAL(timeout()), this, SLOT(timerEnded()));

    ui->progressBar->setMaximum(maximum);
    ui->label->setText("Calibrating...");
}

CalStatus::~CalStatus()
{
    delete ui;
}

void CalStatus::start()
{
    status=0;
    ui->progressBar->setValue(status);
    timerC->start(50);
    this->show();
}

void CalStatus::timerEnded()
{
    if(status<maximum)
    {
        status+=50;
        ui->progressBar->setValue(status);
    }
    else
    {
        timerC->stop();
        emit calEnd();
        this->hide();
    }
}

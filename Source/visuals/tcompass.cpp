#include "tcompass.h"

#include <QDebug>

TCompass::TCompass(QWidget *parent) :
    QWidget(parent)
{
    layout = new QVBoxLayout();
    scene = new QGraphicsScene(this);
    view = new QGraphicsView(scene, this);
    label = new QLabel("No data.\n", this);

    layout->setMargin(0);
    layout->addWidget(view);
    layout->addWidget(label);

    this->setLayout(layout);

    this->setMinimumSize(160, 130);
    this->setMaximumSize(160, 130);
    view->setMinimumSize(142, 97);
    view->setMaximumSize(142, 97);

    label->setAlignment(Qt::AlignCenter);

    QPolygonF pol;
    pol.append(QPointF(-15, 40));
    pol.append(QPointF(15, 40));
    pol.append(QPointF(0, -40));

    needle = scene->addPolygon(pol);
    scene->setBackgroundBrush(Qt::white);

    QPen nPen(QColor(80, 80, 100));
    nPen.setWidth(3);
    needle->setPen(nPen);
    needle->setBrush(QColor(170, 170, 180));

    stallText = scene->addText("Not Defined");
    stallText->setDefaultTextColor(QColor("black"));
    QFont stallFont = stallText->font();
    stallFont.setPixelSize(20);
    stallText->setFont(stallFont);
    stallText->setPos(-stallText->boundingRect().width()/2, -stallText->boundingRect().height()/2);

    stallRect = scene->addRect(stallText->boundingRect());
    stallRect->setPos(stallText->pos());
    stallRect->setBrush(Qt::red);

    stallText->setZValue(stallRect->zValue()+1);

    stallText->hide();
    stallRect->hide();

    stallLabelText = "X axis is vertical.\n"
                     "Using only Gyro.";

    counter = 0;
    heading = 0;
    lastMod = -123;
}

TCompass::~TCompass()
{

}

void TCompass::updateAll()
{
    needle->setRotation(heading);
    label->setText(labelText);
}

void TCompass::setHeading(float angle)
{
    heading = angle;
}

void TCompass::enableStall()
{
    stallText->show();
    stallRect->show();

    label->setText(stallLabelText);
}

void TCompass::disableStall()
{
    stallText->hide();
    stallRect->hide();
}

void TCompass::setStall(bool state)
{
    if(state) enableStall();
    else disableStall();
}

void TCompass::moveColorTo(int r, int g, int b)
{
    QColor act = scene->backgroundBrush().color();
    QColor tar(r, g, b);

    int delta;
    int divider = 10;
    delta = (tar.red()-act.red())/divider;
    tar.setRed(act.red()+delta);
    delta = (tar.green()-act.green())/divider;
    tar.setGreen(act.green()+delta);
    delta = (tar.blue()-act.blue())/divider;
    tar.setBlue(act.blue()+delta);

    scene->setBackgroundBrush(tar);
}

void TCompass::setMovementMod(int mod)
{
    int intensity = counter*10;
    if(intensity > 255) intensity = 255;
    if(mod == 1)
    {
        //still
        if(lastMod != 1) {counter = 1; intensity = 10; disableStall();}
        moveColorTo(200, 150, 50);
        labelText = "Still for "+QString::number(counter)+"\nsamples.";
    }
    else if(mod == 0)
    {
        //move
        if(lastMod != 0) {counter = 1; intensity = 10; disableStall();}
        moveColorTo(100, 200, 100);
        labelText = "Moving for "+QString::number(counter)+"\nsamples.";
    }
    else if(mod == -1)

    {
        //stall
        if(lastMod != -1) {counter = 1; intensity = 10; enableStall();}
        moveColorTo(200, 100, 50);
        labelText = stallLabelText+" ["+QString::number(counter)+"]";
    }

    lastMod = mod;
    counter++;
}

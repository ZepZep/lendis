#ifndef TCOMPASS_H
#define TCOMPASS_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QLabel>
#include <QVBoxLayout>

#include <QGraphicsPolygonItem>
#include <QGraphicsTextItem>
#include <QGraphicsRectItem>

class TCompass : public QWidget
{
    Q_OBJECT

public:
    explicit TCompass(QWidget *parent = 0);
    ~TCompass();

    void updateAll();

    void setHeading(float angle);
    void enableStall();
    void disableStall();
    void setStall(bool state);

private:
    void moveColorTo(int r, int g, int b);

    QGraphicsView *view;
    QGraphicsScene *scene;
    QLabel *label;
    QVBoxLayout *layout;

    QGraphicsPolygonItem *needle;

    QGraphicsTextItem *stallText;
    QGraphicsRectItem *stallRect;
    QString stallLabelText;

    QString labelText;
    float heading;

    int counter;
    int lastMod;

public slots:
    void setMovementMod(int mod);
};

#endif // TCOMPASS_H

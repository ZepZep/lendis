#include "genericnetwork.h"

GenericNetwork::GenericNetwork()
{
    dv0 = new double [6];
    dv1 = new double [6];
    a = new double [3690];
    b_b1 = new double [15];
    b_a = new double [15];

    isTS = false;
}

void GenericNetwork::loadConstants(QString path)
{
    QFile *file = new QFile(path);
    file->open(QIODevice::ReadOnly);

    QString name = QFileInfo(*file).fileName();
    if(name.left(2) == "TD") isTS = false;
    else isTS = true;

    QStringList consts = QString(file->readAll()).split('\n', QString::SkipEmptyParts);

    file->close();

    qDebug() <<"Loading network" <<path;

    if(consts.size() != 3733)
    {
        qDebug() <<"Wrong const sizes!" <<consts.size();
        return;
    }

    int delta = 0;
    for(int i = 0; i<6; i++)
    {
        dv0[i] = ((QString)consts[i+delta]).toDouble();
    }
    delta+=6;
    for(int i = 0; i<6; i++)
    {
        dv1[i] = ((QString)consts[i+delta]).toDouble();
    }
    delta+=6;
    for(int i = 0; i<3690; i++)
    {
        a[i] = ((QString)consts[i+delta]).toDouble();
    }
    delta+=3690;
    for(int i = 0; i<15; i++)
    {
        b_b1[i] = ((QString)consts[i+delta]).toDouble();
    }
    delta+=15;
    for(int i = 0; i<15; i++)
    {
        b_a[i] = ((QString)consts[i+delta]).toDouble();
    }
    delta+=15;
    minmax_const = ((QString)consts[delta]).toDouble();
}

void GenericNetwork::simulate(const double x1[], const double xi1[], double *b_y1, double xf1[])
{
    double xd1[240];

    int i0;
    int i1;
    double b_xd1[246];
    double dv2[41];
    double dv3[41];
    double x[246];
    double tapdelay1[246];
    double b1[15];
    double d0;

    double a1[15];

    mapminmax_apply(xi1, dv1, dv0, -1.0, xd1);
    for (i0 = 0; i0 < 40; i0++) {
        for (i1 = 0; i1 < 6; i1++) {
            b_xd1[i1 + 6 * i0] = xd1[i1 + 6 * i0];
        }
    }

    for (i0 = 0; i0 < 6; i0++) {
        b_xd1[240 + i0] = 0.0;
    }

    b_mapminmax_apply(x1, dv1, dv0, -1.0, *(double (*)[6])&b_xd1[240]);

    for (i0 = 0; i0 < 41; i0++) {
        dv2[i0] = 40.0 - (double)i0;
    }

    b_mod(dv2, 41.0, dv3);
    for (i0 = 0; i0 < 41; i0++) {
        for (i1 = 0; i1 < 6; i1++) {
            x[i1 + 6 * i0] = b_xd1[i1 + 6 * ((int)(dv3[i0] + 1.0) - 1)];
        }
    }

    memcpy(&tapdelay1[0], &x[0], 246U * sizeof(double));
    for (i0 = 0; i0 < 15; i0++) {
        d0 = 0.0;
        for (i1 = 0; i1 < 246; i1++) {
            d0 += a[i0 + 15 * i1] * tapdelay1[i1];
        }

        b1[i0] = b_b1[i0] + d0;
    }

    tansig_apply(b1, a1);

    d0 = 0.0;
    for (i0 = 0; i0 < 15; i0++) {
        d0 += b_a[i0] * a1[i0];
    }

    if(isTS)
        *b_y1 = mapminmax_reverse(b_tansig_apply(minmax_const + d0), 1.0, -1.0, -1.0);
    else
        *b_y1 = mapminmax_reverse(minmax_const + d0, 1.0, -1.0, -1.0);

    for (i0 = 0; i0 < 39; i0++) {
        for (i1 = 0; i1 < 6; i1++) {
            xf1[i1 + 6 * i0] = xi1[i1 + 6 * (1 + i0)];
        }
    }

    for (i0 = 0; i0 < 6; i0++) {
        xf1[234 + i0] = x1[i0];
    }
}

void GenericNetwork::b_mapminmax_apply(const double x[], const double settings_gain[], const double settings_xoffset[], double settings_ymin, double y[])
{
    int k;
    for (k = 0; k < 6; k++)
        y[k] = (x[k] - settings_xoffset[k]) * settings_gain[k] + settings_ymin;
}

void GenericNetwork::mapminmax_apply(const double x[], const double settings_gain[], const double settings_xoffset[], double settings_ymin, double y[])
{
    int ak;
    int ck;
    double cv[6];
    int k;
    double a[240];

    ak = 0;
    for (ck = 0; ck < 236; ck += 6) {
        for (k = 0; k < 6; k++) {
            cv[k] = x[ak + k] - settings_xoffset[k];
        }

        for (k = 0; k < 6; k++) {
            y[ck + k] = cv[k];
        }

        ak += 6;
    }

    memcpy(&a[0], &y[0], 240U * sizeof(double));
    ak = 0;
    for (ck = 0; ck < 236; ck += 6) {
        for (k = 0; k < 6; k++) {
            cv[k] = a[ak + k] * settings_gain[k];
        }

        for (k = 0; k < 6; k++) {
            y[ck + k] = cv[k];
        }

        ak += 6;
    }

    memcpy(&a[0], &y[0], 240U * sizeof(double));
    ak = 0;
    for (ck = 0; ck < 236; ck += 6) {
        for (k = 0; k < 6; k++) {
            cv[k] = a[ak + k] + settings_ymin;
        }

        for (k = 0; k < 6; k++) {
            y[ck + k] = cv[k];
        }

        ak += 6;
    }
}

double GenericNetwork::mapminmax_reverse(double y, double settings_gain, double settings_xoffset, double settings_ymin)
{
    return (y - settings_ymin) / settings_gain + settings_xoffset;
}

void GenericNetwork::tansig_apply(const double n[], double a[])
{
    int i;
    for (i = 0; i < 15; i++)
        a[i] = 2.0 / (1.0 + exp(-2.0 * n[i])) - 1.0;
}

double GenericNetwork::b_tansig_apply(double n)
{
    return 2.0 / (1.0 + exp(-2.0 * n)) - 1.0;
}

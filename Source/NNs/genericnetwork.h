#ifndef GENERICNETWORK_H
#define GENERICNETWORK_H

#include <QString>
#include <QFile>
#include <QDebug>

#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <QFileInfo>

#include "mod.h"

class GenericNetwork
{
public:
    GenericNetwork();

    void loadConstants(QString path);
    void simulate(const double x1[6], const double xi1[240], double *b_y1, double xf1[240]);

private:
    static void b_mapminmax_apply(const double x[6], const double settings_gain[6], const double settings_xoffset[6], double settings_ymin, double y[6]);
    static void mapminmax_apply(const double x[240], const double settings_gain[6], const double settings_xoffset[6], double settings_ymin, double y[240]);
    static double mapminmax_reverse(double y, double settings_gain, double settings_xoffset, double settings_ymin);
    static void tansig_apply(const double n[15], double a[15]);
    static double b_tansig_apply(double n);

//    double *xd1;
//    int i0;
//    int i1;
//    double *b_xd1;
//    double *dv2;
//    double *dv3;
//    double *x;
//    double *tapdelay1;
//    double *b1;
//    double d0;
//    double *a1;

    double * dv0;
    double *dv1;
    double *a;
    double *b_b1;
    double *b_a;
    double minmax_const;

    bool isTS;
};

#endif // GENERICNETWORK_H

/*
 * File: mod.h
 *
 * MATLAB Coder version            : 2.7
 * C/C++ source code generated on  : 18-Jan-2016 14:50:19
 */

#ifndef __MOD_H__
#define __MOD_H__

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
//#include "rtwtypes.h"
//#include "brainNN_types.h"

/* Function Declarations */
extern void b_mod(const double x[41], double y, double r[41]);

#endif

/*
 * File trailer for mod.h
 *
 * [EOF]
 */

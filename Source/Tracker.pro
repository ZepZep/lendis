#-------------------------------------------------
#
# Project created by QtCreator 2015-08-04T13:52:50
#
#-------------------------------------------------

QT       += core gui printsupport serialport opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += gui
TARGET = Tracker
TEMPLATE = app

SOURCES += main.cpp\
    visuals/qcustomplot.cpp \
    visuals/glcubewidget.cpp \
    visuals/calstatus.cpp \
    dealers/calibrator.cpp \
    dealers/derotator.cpp \
    dealers/integrator.cpp \
    dealers/reciever.cpp \
    ui/cubedialog.cpp \
    ui/packformater.cpp \
    ui/settings.cpp \
    ui/vizualizer.cpp \
    utils/brain.cpp \
    utils/pplotgroup.cpp \
    utils/putils.cpp \
    ui/customtab.cpp \
    NNs/mod.cpp \
    visuals/tcompass.cpp \
    dealers/filehandler.cpp \
    NNs/genericnetwork.cpp

FORMS += \
    visuals/calstatus.ui \
    ui/cubedialog.ui \
    ui/packformater.ui \
    ui/settings.ui \
    ui/vizualizer.ui \
    ui/customtab.ui \
    dealers/filehandler.ui

HEADERS += \
    visuals/qcustomplot.h \
    visuals/glcubewidget.h \
    visuals/calstatus.h \
    dealers/calibrator.h \
    dealers/derotator.h \
    dealers/integrator.h \
    dealers/reciever.h \
    ui/cubedialog.h \
    ui/packformater.h \
    ui/settings.h \
    ui/vizualizer.h \
    utils/brain.h \
    utils/pplotgroup.h \
    utils/putils.h \
    ui/customtab.h \
    NNs/mod.h \
    visuals/tcompass.h \
    dealers/filehandler.h \
    NNs/genericnetwork.h


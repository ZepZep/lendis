#include "brain.h"

Brain::Brain()
{
    for(int i = 0; i<240; i+=6)
    {
        hist[i]=0;
        hist[i+1]=0;
        hist[i+2]=0;
        hist[i+3]=0;
        hist[i+4]=0;
        hist[i+5]=1;
    }

    kalman = new Kalman(0.1, 0.5, 25, 10);
    network = new GenericNetwork;
    presets = new QSettings("config.ini", QSettings::IniFormat);

    loadSettings();
}

Brain::~Brain()
{

}

int Brain::decide(QVector3D acc, QVector3D gyr, float &nnOut, float &nnKalOut)
{
    input[0] = gyr.x();
    input[1] = gyr.y();
    input[2] = gyr.z();
    input[3] = acc.x();
    input[4] = acc.y();
    input[5] = acc.z();

    double output = 0;

    network->simulate(input, hist, &output, hist);

    nnOut = output;
    nnKalOut = kalman->getrValue(output);

    if(nnKalOut>boundary) return 0;
    else return 1;
}

void Brain::setNetwork(QString path)
{
    network->loadConstants(path);
}

void Brain::loadSettings()
{
    presets->beginGroup("neural");
    boundary = presets->value("boundary", 0).toDouble();
    p = presets->value("p", 0.1).toDouble();
    q = presets->value("q", 0.5).toDouble();
    r = presets->value("r", 25).toDouble();
    presets->endGroup();

    kalman->reset(p, q, r, 10);
}

#include "putils.h"
#include <QDebug>

Avrg::Avrg(int histLenghth)
{
    reset(histLenghth);
}

float Avrg::getrAvr(float arg)
{
    if(isInfinite)
    {
        fInfVal = (fInfVal*fInfLen + arg) / (fInfLen+1);
        fInfLen++;
        return fInfVal;
    }
    else
    {
        fSuma += arg;
        fSuma -= fList.at(fWhere%history);
        fList.replace(fWhere%history, arg);
        fWhere++;

        if(fWhere > history) return fSuma/history;
        else return fSuma/fWhere;
    }
}

QVector3D Avrg::getrAvr(QVector3D arg)
{
    if(isInfinite)
    {
        vInfVal = (vInfVal*vInfLen + arg) / (vInfLen+1);
        vInfLen++;
        return vInfVal;
    }
    else
    {
        vSuma += arg;
        vSuma -= vList.at(vWhere%history);
        vList.replace(vWhere%history, arg);
        vWhere++;

        if(vWhere > history) return vSuma/history;
        else return vSuma/vWhere;
    }
}

void Avrg::reset()
{
    fInfLen = 0;
    fList.fill(0);
    fWhere = 0;
    fSuma = 0;
    fInfVal = 0;

    vInfLen = 0;
    vList.fill(QVector3D(0, 0, 0));
    vWhere = 0;
    vSuma = QVector3D(0, 0, 0);
    vInfVal = QVector3D(0, 0, 0);
}

void Avrg::reset(int histLenghth)
{
    if(histLenghth == 0) isInfinite = true;
    else isInfinite = false;
    history = histLenghth;
    fList.resize(history);
    vList.resize(history);

    reset();
}


Kalman::Kalman(float kp, float kq, float kr, int avgLenght)
{
    avrg = new Avrg(avgLenght);
    reset(kp, kq, kr, avgLenght);
}

void Kalman::reset(float kp, float kq, float kr, int avgLenght)
{
    counter = 0;
    vCounter = 0;
    initP = kp;
    p = kp;
    vp = QVector3D(kp, kp, kp);
    initQ = kq;
    q = kq;
    vq = QVector3D(kq, kq, kq);
    initR = kr;
    r = kr;
    vr = QVector3D(kr, kr, kr);
    avgLen = avgLenght;
    avrg->reset(avgLenght);
}

void Kalman::reset()
{
    reset(initP, initQ, initR, avgLen);
}

float Kalman::getrValue(float arg)
{
    if(counter < avgLen)
    {
        avg = avrg->getrAvr(arg);
    }
    else
    {
        p=p+q;
        k=p/(p+r);
        avg += k*(arg-avg);
        p=(1-k)*p;
    }
    counter++;
    return avg;
}

QVector3D Kalman::getrValue(QVector3D arg)
{
    if(vCounter < avgLen)
    {
        vAvg = avrg->getrAvr(arg);
    }
    else
    {
        vp=vp+vq;
        vk=vp/(vp+vr);
        vAvg += vk*(arg-vAvg);

        vp=(QVector3D(1, 1, 1)-vk)*vp;
    }
    vCounter++;
    return vAvg;
}

//float abs(float x)
//{
//    if(x<0) return -x;
//    else return x;
//}

QVector3D eulerFromQuat(QQuaternion quat)
{
    float phi, theta, psi;
    theta = asin(-2*quat.x()*quat.z()+2*quat.y()*quat.scalar());
    phi = atan2((2*quat.y()*quat.z()+2*quat.x()*quat.scalar())/cos(theta),
                (1-2*quat.x()*quat.x()-2*quat.y()*quat.y())/cos(theta));
    psi = atan2((2*quat.y()*quat.x()+2*quat.z()*quat.scalar())/cos(theta),
                (1-2*quat.z()*quat.z()-2*quat.y()*quat.y())/cos(theta));

    return QVector3D(phi, theta, psi);
}

float headingFromQuat(QQuaternion quat)
{
    QVector3D start(1,0,0);
    start = quat.rotatedVector(start);
    float euler = eulerFromQuat(quat).z();
    return euler;
}

#ifndef PUTILS_H
#define PUTILS_H

#include <QVector>
#include <QVector3D>
#include <QQuaternion>

#include <math.h>

class Avrg
{
public:
    explicit Avrg(int histLenghth);
    float getrAvr(float arg);
    QVector3D getrAvr(QVector3D arg);
    void reset();
    void reset(int histLenghth) ;
protected:
    int history;
    QVector<float> fList;
    float fSuma;
    int fWhere;
    QVector<QVector3D> vList;
    QVector3D vSuma;
    int vWhere;
    bool isInfinite;
    int fInfLen;
    int vInfLen;
    float fInfVal;
    QVector3D vInfVal;
};

class Kalman
{
public:
    explicit Kalman(float kp, float kq, float kr, int avgLenght);
    void reset(float kp, float kq, float kr, int avgLenght) ;
    void reset();
    float getrValue(float arg);
    QVector3D getrValue(QVector3D arg);


private:
    float p, q, r;
    float initP, initQ, initR;
    int avgLen;
    Avrg *avrg;

    float avg;
    int counter;
    float k;

    int vCounter;
    QVector3D vAvg;
    QVector3D vp, vq, vr, vk;
};

//float abs(float x);
QVector3D eulerFromQuat(QQuaternion quat);
float headingFromQuat(QQuaternion quat);

#endif // PUTILS_H

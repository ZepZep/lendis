#ifndef BRAIN_H
#define BRAIN_H

#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <QVector3D>

#include <QSettings>

#include "utils/putils.h"

#include "NNs/genericnetwork.h"

class Brain
{
public:
    Brain();
    ~Brain();
    int decide(QVector3D acc, QVector3D gyr, float &nnOut, float &nnKalOut);

    void setNetwork(QString path);

    void loadSettings();


private:
    void netFcn(const double x1[2], const double xi1[20], double *b_y1, double xf1[20]);

    GenericNetwork *network;
    Kalman *kalman;

    QSettings *presets;

    double p, q, r, boundary;

    double hist[240];
    double input[6];




};

#endif // BRAIN_H

#include "pplotgroup.h"

PPlotGroup::PPlotGroup(QString name, QWidget *parent):
    QGroupBox(parent)
{
    this->setTitle(name);
    plot = new QCustomPlot(this);
    layMain = new QHBoxLayout;
    layLabels = new QVBoxLayout;
    spacer = new QSpacerItem(0, 0, QSizePolicy::Fixed, QSizePolicy::Maximum);

    this->setMinimumSize(QSize(400, 150));
    plot->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    layMain->setMargin(2);

    layBounds = new QHBoxLayout;
    lEMin = new QLineEdit(this);
    lEMin->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
    lEMin->hide();
    lEMax = new QLineEdit(this);
    lEMax->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
    lEMax->hide();

    connect(lEMin, SIGNAL(returnPressed()), this, SLOT(changeBounds()));
    connect(lEMin, SIGNAL(editingFinished()), this, SLOT(changeBounds()));
    connect(lEMax, SIGNAL(returnPressed()), this, SLOT(changeBounds()));
    connect(lEMax, SIGNAL(editingFinished()), this, SLOT(changeBounds()));

    valueX = new QLabel("000", this);
    valueY = new QLabel("000", this);
    valueZ = new QLabel("000", this);

    QFont monoFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    valueX->setFont(monoFont);
    valueY->setFont(monoFont);
    valueZ->setFont(monoFont);

    layBounds->addWidget(lEMin);
    layBounds->addWidget(lEMax);

    layLabels->addWidget(valueX);
    layLabels->addWidget(valueY);
    layLabels->addWidget(valueZ);
    layLabels->addLayout(layBounds);
    layLabels->addSpacerItem(spacer);

    layLabels->setAlignment(valueX, Qt::AlignRight);
    layLabels->setAlignment(valueY, Qt::AlignRight);
    layLabels->setAlignment(valueZ, Qt::AlignRight);

    valueX->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred));
    valueY->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred));
    valueZ->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred));

    layMain->addWidget(plot);
    layMain->addLayout(layLabels);

    this->setLayout(layMain);

    setRangeX(-5, 5);
    setRangeY(-5, 5);

    redPen.setColor(QColor("red"));
    greenPen.setColor(QColor("green"));

    plot->clearGraphs();

    plot->addGraph();
    plot->addGraph();
    plot->addGraph();

    plot->graph(1)->setPen(greenPen);
    plot->graph(2)->setPen(redPen);

    ux = 0;
    uy = 0;
    uz = 0;

    showBounds();
}

PPlotGroup::~PPlotGroup()
{

}

void PPlotGroup::replot()
{
    cleanUp();

    valueX->setText(getLabel(ux));
    valueY->setText(getLabel(uy));
    valueZ->setText(getLabel(uz));

    plot->replot();
}

void PPlotGroup::cleanUp()
{
    plot->graph(0)->removeDataAfter(rangeUpperX+2);
    plot->graph(1)->removeDataAfter(rangeUpperX+2);
    plot->graph(2)->removeDataAfter(rangeUpperX+2);

    plot->graph(0)->removeDataBefore(rangeLowerX-2);
    plot->graph(1)->removeDataBefore(rangeLowerX-2);
    plot->graph(2)->removeDataBefore(rangeLowerX-2);

    plot->xAxis->setRange(rangeLowerX, rangeUpperX);
    plot->yAxis->setRange(rangeLowerY, rangeUpperY);
}

void PPlotGroup::clearData()
{
    plot->graph(0)->clearData();
    plot->graph(1)->clearData();
    plot->graph(2)->clearData();
}

void PPlotGroup::setRangeX(float lower, float upper)
{
    rangeLowerX=lower;
    rangeUpperX=upper;
}

void PPlotGroup::setRangeY(float lower, float upper)
{
    rangeLowerY=lower;
    lEMin->setText(QString::number(lower));
    rangeUpperY=upper;
    lEMax->setText(QString::number(upper));
}

void PPlotGroup::addData(float key, float x, float y, float z)
{
    plot->graph(0)->addData(key, x);
    plot->graph(1)->addData(key, y);
    plot->graph(2)->addData(key, z);

    ux=x;
    uy=y;
    uz=z;
}

void PPlotGroup::addData(float key, QVector3D values)
{
    addData(key, values.x(), values.y(), values.z());
}

float PPlotGroup::abs(float x)
{
    if(x<0) return -x;
    else return x;
}

void PPlotGroup::showBounds()
{
    lEMin->show();
    lEMax->show();
}

bool PPlotGroup::event(QEvent *event)
{
    if(event->type()==QEvent::Resize)
    {
        //QGroupBox::event(event);
        int heig=this->height()-20;
        heig /= 3.5;
        if(heig > 70) heig = 70;
        QFont newFont = valueX->font();
        newFont.setPixelSize(heig);
        valueX->setFont(newFont);
        valueY->setFont(newFont);
        valueZ->setFont(newFont);

        return true;
    }
    else return QGroupBox::event(event);
}

QString PPlotGroup::getLabel(float x)
{
    QString output = "";

    if(x<-999.999) x = -999.999;
    else if(x>999.999) x = 999.999;

    int pocetMezer = 2;
    if(abs(x)>=1) pocetMezer-=(int)std::log10(abs(x));
    for(int i=0; i<pocetMezer; i++) output+=" ";

    if(x<0) output += "-";
    else output += " ";

    output += QString::number(abs(x), 'f', 3);

    return output;
}

void PPlotGroup::changeBounds()
{
    setRangeY(lEMin->text().toFloat(), lEMax->text().toFloat());
    replot();
}

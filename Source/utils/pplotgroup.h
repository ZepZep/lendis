#ifndef PPLOTGROUP_H
#define PPLOTGROUP_H

#include <cmath>
#include <QGroupBox>
#include <QLayout>
#include <QLabel>
#include <QLineEdit>

#include "visuals/qcustomplot.h"

class PPlotGroup : public QGroupBox
{
    Q_OBJECT

public:
    PPlotGroup(QString name, QWidget *parent = 0);
    ~PPlotGroup();

    void replot();
    void cleanUp();
    void clearData();
    void setRangeX(float lower, float upper);
    void setRangeY(float lower, float upper);
    void addData(float key, float x, float y, float z);
    void addData(float key, QVector3D values);
    float abs(float x);
    void showBounds();

private:
    float rangeUpperX, rangeLowerX, rangeUpperY, rangeLowerY;
    float ux, uy, uz;

    QCustomPlot *plot;
    QHBoxLayout *layMain;
    QVBoxLayout *layLabels;
    QSpacerItem *spacer;

    QLabel *valueX;
    QLabel *valueY;
    QLabel *valueZ;

    QPen redPen, greenPen;

    QHBoxLayout *layBounds;
    QLineEdit *lEMin, *lEMax;

    bool event(QEvent *event);
    QString getLabel(float x);

private slots:
    void changeBounds();
};

#endif // PPLOTGROUP_H
